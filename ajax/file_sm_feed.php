 <?php error_reporting(0);?>

 <?php session_start(); ?>

 <head>
 	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

 	<!-- Bootstrap 3.3.7 -->

 	<link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">

 	<!-- Font Awesome -->

 	<link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">

 	<!-- Ionicons -->

 	<link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">

 	<!-- dtt -->

 	<link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

 	<!-- Theme style -->

 	<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins

  	folder instead of downloading all of them to reduce the load. -->

  	<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
  	<link rel="stylesheet" href="../dist/css/custom.css">

  	<!-- Morris chart -->

  	<link rel="stylesheet" href="../bower_components/morris.js/morris.css">

  	<!-- jvectormap -->

  	<link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">

  	<!-- Date Picker -->

  	<link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

  	<!-- Daterange picker -->

  	<link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">

  	<!-- bootstrap wysihtml5 - text editor -->

  	<link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">


  </head>

  <body>
  	
  	<div class="container">
  		<legend><h3>File Lampiran</h3></legend>

  		<?php
  		include("../php/koneksi.php");
  		include("../php/fungsi.php");
  		bacaDisp($_GET["id_disposisi"]);
  		$res = mysql_query("SELECT * FROM myapp_filetable_suratmasuk WHERE id_surat_masuk='" . $_GET["id"] . "'");
  		$num = mysql_num_rows($res);

  		if($num <= 0){
  			   echo "<center style='padding-top:20px;'><span class='well'>TIDAK ADA FILE LAMPIRAN SURAT MASUK</span></center>";
  		}else{?>
  		<table class="table table-bordered table-striped detail table-hover">
  			<tr>
  				<th>
  					NAMA FILE
  				</th>
  				<th>
  					KETERANGAN FILE
  				</th>
  				<th>
  					UNDUH
  				</th>
  			</tr>
  			<?php
  			while($ds = mysql_fetch_array($res)){
  				?>		<tr>
  					<td>
  						<?php echo($ds["nama_file"]) ?>
  					</td>
  					<td>

  						<?php 
  						echo($ds["keterangan"])
  						?>
  					</td>
  					<td>

  						<a class="linktambahan btn btn-sm btn-warning" target="_blank" href="../uploaded/sm/<?php echo($ds["nama_file"]); ?>"><i class="fa fa-download"></i></a>
  					</td>
  				</tr>
  				<?php
  			}
  			echo '</table>';
  		}	
  		?> 

  	</div>
  </table>