<head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->

  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">

  <!-- Font Awesome -->

  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">

  <!-- Ionicons -->

  <!--link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">

  <!-- dtt -->

  <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- Theme style -->

  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins

  folder instead of downloading all of them to reduce the load. -->

  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

  <!-- Morris chart -->

  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">

  <!-- jvectormap -->

  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">

  <!-- Date Picker -->

  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

  <!-- Daterange picker -->

  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="../dist/css/custom.css">


  <!-- bootstrap wysihtml5 - text editor -->

  <!--link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"-->


</head>

<body>
    
 <div class="container">
  
	<legend><h3>Detail Surat Masuk</h3></legend>

    <form name="frm" action="../php/edit_surat_masuk.php" method="post">

            <?php
                error_reporting(0);
            

                include("../php/koneksi.php");

                include("../php/fungsi.php");

                bacaDisp($_GET["id_disposisi"]);

                $res_sm = mysql_query("SELECT 

                                        	a.*, b.unit_kerja, CONCAT('(', c.kode_masalah, ') ', c.masalah) AS masalah,

                                        	CONCAT('(', d.kode, ') ', d.jenis_surat) AS jenis_surat

                                        FROM 

                                        	myapp_maintable_suratmasuk a

                                        	LEFT JOIN myapp_reftable_unitkerja b ON a.id_skpd_pengirim = b.id_unit_kerja

                                        	LEFT JOIN myapp_reftable_masalah c ON a.id_masalah = c.id_masalah

                                        	LEFT JOIN myapp_reftable_jenissurat d ON a.id_jenis_surat = d.id_jenis_surat

                                        WHERE 

                                        	a.id_surat_masuk='" . $_GET["id"] . "'");

                $ds_sm = mysql_fetch_array($res_sm); 
            ?>

            <input type="hidden" name="id" value="<?php echo($_GET["id"]); ?>" />

            <table id="example2" class="detail listingtable table table-bordered table-striped">

                <tr>

                    <td width='20%'>Nomor Surat</td>

                    <td width='10px'>:</td>

                    <td><b><?php echo($ds_sm["no_surat"]); ?></b></td>

                </tr>

                <tr>

                    <td width='20%'>Tanggal Surat</td>

                    <td width='10px'>:</td>

                    <td><b><?php echo($ds_sm["tgl_surat"]); ?></b></td>

                </tr>

                <tr>

                    <td width='20%'>Tanggal Terima</td>

                    <td width='10px'>:</td>

                    <td><b><?php echo($ds_sm["tgl_terima"]); ?></b></td>

                </tr>

                <tr>

                    <td width='20%'>Perihal</td>

                    <td width='10px'>:</td>

                    <td><b><?php echo($ds_sm["perihal_surat"]); ?></b></td>

                </tr>

                <!-- <tr>

                    <td width='20%'>Pengirim</td>

                    <td width='10px'>:</td>

                    <td><b><?php echo($ds_sm["pengirim_surat"]); ?></b></td>

                </tr> -->

                <tr>

                    <td width='20%'>Alamat Pengirim</td>

                    <td width='10px'>:</td>

                    <td><b><?php echo($ds_sm["alamat_pengirim"]); ?></b></td>

                </tr>

                <tr>

                    <td width='20%'>Judul Surat</td>

                    <td width='10px'>:</td>

                    <td><b><?php echo($ds_sm["judul_surat"]); ?></b></td>

                </tr>

                <tr>

                    <td width='20%'>Deskripsi Surat</td>

                    <td width='10px'>:</td>

                    <td><b><?php echo($ds_sm["deskripsi_surat"]); ?></b></td>

                </tr>

                <tr>

                    <td width='20%'>Catatan Tambahan</td>

                    <td width='10px'>:</td>

                    <td><b><?php echo($ds_sm["catatan"]); ?></b></td>

                </tr>

                <tr>

                    <td width='20%'>SKPD / Unit Pengirim</td>

                    <td width='10px'>:</td>

                    <td><b><?php echo($ds_sm["unit_kerja"]); ?></b></td>

                </tr>

                <tr>

                    <td width='20%'>Masalah</td>

                    <td width='10px'>:</td>

                    <td><b><?php echo($ds_sm["masalah"]); ?></b></td>

                </tr>

                <tr>

                    <td width='20%'>Sub Masalah</td>

                    <td width='10px'>:</td>

                    <td><b><?php echo($ds_sm["jenis_surat"]); ?></b></td>

                </tr>

                <tr>

                    <td width='20%'>Harus Selesai Pada<br /><span class="footnote">*) Kosong jika tidak perlu ditindak lanjuti</span></td>

                    <td width='10px'>:</td>

                    <td><b><?php echo($ds_sm["harus_selesai"]); ?></b></td>

                </tr>

            <!-- <tr>

                    <td width='20%'>Indeks</td>

                    <td width='10px'>:</td>

                    <td><b><?php echo($ds_sm["indeks"]); ?></b></td>

                </tr>

                <tr>

                    <td width='20%'>Kode</td>

                    <td width='10px'>:</td>

                    <td><b><?php echo($ds_sm["kode"]); ?></b></td>

                </tr> -->

            </table>
     </form>

 </div>
</body>