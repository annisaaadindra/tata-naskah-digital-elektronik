<script>
  new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'suratkeluarbelumsiap',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: [
     <?php
    $conn_smt = mysql_query('select * from myapp_maintable_suratkeluar where status < 4 order OR tgl_surat <> "0000-00-00" by tgl_surat asc');
    $tahun = '';
    $i = 0;
    $j = 0;
    $tot_smt = mysql_num_rows($conn_smt);
    while($data_smt = mysql_fetch_array($conn_smt)){
      $j++;
      $arrtgl = explode('-', $data_smt['tgl_surat']); 
      if ($tahun != $arrtgl[0]){
        if($i > 0)
        {
          echo 'jumlah: '.$i.' },'; 
          
          $i = 0;
        }

        echo '{ tahun: "'.$arrtgl[0].'",';
        $i++; 

        if ($tot_smt  == 1)
        {
          echo 'jumlah: '.$i.' }'; 
        }

      }
      else{
        $i++; 
        if($j == $tot_smt)
          {
            echo 'jumlah: '.$i.' }';  
          }
      }

      $tahun = $arrtgl[0];
    } 
  ?>
  ],
  // The name of the data record attribute that contains x-values.
  xkey: 'tahun',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['jumlah'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Jumlah']
});

  new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'suratmasukbelumsiap',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: [
     <?php
    $conn_smt = mysql_query('select * from myapp_maintable_suratmasuk where status < 5 order by tgl_surat asc');
    $tahun = '';
    $i = 0;
    $j = 0;
    $tot_smt = mysql_num_rows($conn_smt);
    while($data_smt = mysql_fetch_array($conn_smt)){
      $j++;
      $arrtgl = explode('-', $data_smt['tgl_surat']); 
      if ($tahun != $arrtgl[0]){
        if($i > 0)
        {
          echo 'jumlah: '.$i.' },'; 
          
          $i = 0;
        }

        echo '{ tahun: "'.$arrtgl[0].'",';
        $i++;

        if ($tot_smt  == 1)
        {
          echo 'jumlah: '.$i.' }'; 
        }
      }
      else{
        $i++;
        if($j == $tot_smt)
          {
            echo 'jumlah: '.$i.' }';  
          }
      }
      $tahun = $arrtgl[0];
    }
  ?>
  ],
  // The name of the data record attribute that contains x-values.
  xkey: 'tahun',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['jumlah'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Jumlah']
});

  new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'suratmasukpertahun',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: [
     <?php
    $conn_smt = mysql_query('select * from myapp_maintable_suratmasuk order by tgl_surat asc');
    $tahun = '';
    $i = 0;
    $j = 0;
    $tot_smt = mysql_num_rows($conn_smt);
    while($data_smt = mysql_fetch_array($conn_smt)){
      $j++;
      $arrtgl = explode('-', $data_smt['tgl_surat']); 
      if ($tahun != $arrtgl[0]){
        if($i > 0)
        {
          echo 'jumlah: '.$i.' },'; 
          
          $i = 0;
        }

        echo '{ tahun: "'.$arrtgl[0].'",';
        $i++;

        if ($tot_smt  == 1)
        {
          echo 'jumlah: '.$i.' }'; 
        }
      }
      else{
        $i++;
        if($j == $tot_smt)
          {
            echo 'jumlah: '.$i.' }';  
          }
      }
      $tahun = $arrtgl[0];
    }
  ?>
  ],
  // The name of the data record attribute that contains x-values.
  xkey: 'tahun',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['jumlah'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Jumlah']
});

  new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'suratkeluarpertahun',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: [
     <?php
    $conn_skt = mysql_query('select * from myapp_maintable_suratkeluar where tgl_surat <> "0000-00-00" and status > 3 order by tgl_surat asc');
    $tahun = '';
    $i = 0;
    $j = 0; 
    $tot_skt = mysql_num_rows($conn_skt);
    while($data_skt = mysql_fetch_array($conn_skt)){
      $j++;
      $arrtgl = explode('-', $data_skt['tgl_surat']); 
      if ($tahun != $arrtgl[0]){
        if($i > 0)
        {
          echo 'jumlah: '.$i.' },'; 
          
          $i = 0;

        }

        echo '{ tahun: "'.$arrtgl[0].'",';
        $i++;

        if ($tot_skt  == 1)
        {
          echo 'jumlah: '.$i.' }'; 
        }
      }
      else{
        $i++;
        if($j == $tot_skt)
          {
            echo 'jumlah: '.$i.' }';  
          }
      }
      $tahun = $arrtgl[0];
    }
  ?>
  ],
  // The name of the data record attribute that contains x-values.
  xkey: 'tahun',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['jumlah'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Jumlah']
});

  new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'suratmasukfoluppertahun',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: [
     <?php
    $conn_skt = mysql_query('select * from myapp_maintable_suratmasuk where status = 5 order by tgl_surat asc');
    $tahun = '';
    $i = 0;
    $j = 0; 
    $tot_skt = mysql_num_rows($conn_skt);
    while($data_skt = mysql_fetch_array($conn_skt)){
      $j++;
      $arrtgl = explode('-', $data_skt['tgl_surat']); 
      if ($tahun != $arrtgl[0]){
        if($i > 0)
        {
          echo 'jumlah: '.$i.' },'; 
          
          $i = 0;

        }

        echo '{ tahun: "'.$arrtgl[0].'",';
        $i++;

        if ($tot_skt  == 1)
        {
          echo 'jumlah: '.$i.' }'; 
        }
      }
      else{
        $i++;
        if($j == $tot_skt)
          {
            echo 'jumlah: '.$i.' }';  
          }
      }
      $tahun = $arrtgl[0];
    }
  ?>
  ],
  // The name of the data record attribute that contains x-values.
  xkey: 'tahun',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['jumlah'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Jumlah']
});
</script>