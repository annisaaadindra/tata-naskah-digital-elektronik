<div class="content-wrapper">

  <!-- Content Header (Page header) -->

  <section class="content-header">

    <h1>

      BERANDA


    </h1>

    <ol class="breadcrumb">

      <li><a href="./"><i class="fa fa-dashboard"></i> Beranda</a></li> 
    </ol>

  </section>



  <!-- Main content -->

  <?php

  $connmasuk = mysql_query("select * from myapp_maintable_suratmasuk where status <> 5");

  $datamasuk = mysql_num_rows($connmasuk);



  $connkeluar = mysql_query("select * from myapp_maintable_suratkeluar where status < 4");

  $datakeluar = mysql_num_rows($connkeluar);

  ?>

  <section class="content">

    <!-- Small boxes (Stat box) -->

    <div class="row">

      <div class="col-lg-6 col-xs-6">

        <!-- small box -->

        <div class="small-box bg-aqua">

          <div class="inner">

            <h3><?= $datamasuk?></h3>



            <p>Surat Masuk Yang Sedang Diproses</p>

          </div>

          <div class="icon">

            <i class="ion ion-android-mail"></i>

          </div>

          <div class="small-box-footer">Data bersifat realtime</div>

        </div>

      </div>

      <div class="col-lg-6 col-xs-6">

        <!-- small box -->

        <div class="small-box bg-green">

          <div class="inner">

            <h3><?= $datakeluar?></h3>



            <p>Surat Keluar Yang Sedang Diproses</p>

          </div>

          <div class="icon">

            <i class="ion ion-android-mail"></i>

          </div>

          <div class="small-box-footer">Data bersifat realtime</div>

        </div>

      </div>

      <!-- ./col --> 

      <!-- ./col --> 

      <!-- ./col --> 

      <!-- ./col -->

    </div>

    <!-- /.row -->

    <!-- Main row -->



    <div class="row">

      <!-- Left col -->

      <section class="col-lg-6 connectedSortable">

        <!-- Custom tabs (Charts with tabs)-->

        <div class="nav-tabs-custom">

          <!-- Tabs within a box -->

          <ul class="nav nav-tabs pull-right"> 

            <li class="pull-left header"><i class="fa fa-inbox"></i><small> Jumlah Surat Masuk Per Tahun</small></li>

          </ul>

          <div class="tab-content no-padding">

            <!-- Morris chart - Sales -->

            <div id="suratmasukpertahun" style="height: 250px;"></div>

          </div>

        </div> 



        <div class="nav-tabs-custom">

          <!-- Tabs within a box -->

          <ul class="nav nav-tabs pull-right"> 

            <li class="pull-left header"><i class="fa fa-inbox"></i><small> Jumlah Surat Keluar Per Tahun </small></li>

          </ul>

          <div class="tab-content no-padding">

            <!-- Morris chart - Sales -->

            <div id="suratkeluarpertahun" style="height: 250px;"></div>

          </div>

        </div> 

        <div class="nav-tabs-custom">

          <!-- Tabs within a box -->

          <ul class="nav nav-tabs pull-right"> 

            <li class="pull-left header"><i class="fa fa-inbox"></i> <small>Jumlah Surat Keluar Belum Selesai di Proses</small></li>

          </ul>

          <div class="tab-content no-padding">

            <!-- Morris chart - Sales -->

            <div id="suratkeluarbelumsiap" style="height: 250px;"></div>

            

          </div>

        </div> 



        <!-- /.nav-tabs-custom -->



        <!-- Chat box --> 



      </section>

      <!-- /.Left col -->

      <!-- right col (We are only adding the ID to make the widgets sortable)-->

      <section class="col-lg-6 connectedSortable">



        <!-- Map box --> 



        <!-- Calendar -->

        



        <div class="nav-tabs-custom">

          <!-- Tabs within a box -->

          <ul class="nav nav-tabs pull-right"> 

            <li class="pull-left header"><i class="fa fa-inbox"></i> <small>Jumlah Surat Masuk yang Selesai Di Follow Up Per Tahun</small></li>

          </ul>

          <div class="tab-content no-padding">

            <!-- Morris chart - Sales -->

            <div id="suratmasukfoluppertahun" style="height: 250px;"></div>

            

          </div>

        </div> 

        <div class="nav-tabs-custom">

          <!-- Tabs within a box -->

          <ul class="nav nav-tabs pull-right"> 

            <li class="pull-left header"><i class="fa fa-inbox"></i> <small>Jumlah Surat Masuk Belum Selesai di Proses</small></li>

          </ul>

          <div class="tab-content no-padding">

            <!-- Morris chart - Sales -->

            <div id="suratmasukbelumsiap" style="height: 250px;"></div>

            

          </div>

        </div> 

        <div class="box box-solid bg-green-gradient">

          <div class="box-header">

            <i class="fa fa-calendar"></i>



            <h3 class="box-title">Kalender</h3>

            <!-- tools box -->

            <div class="pull-right box-tools">

              <!-- button with a dropdown -->

              <div class="btn-group"> 

                <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>

                </button>

                <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i>

                </button>

              </div>

              <!-- /. tools -->

            </div>

            <!-- /.box-header -->

            <div class="box-body no-padding">

              <!--The calendar -->

              <div id="calendar" style="width: 100%"></div>

            </div>

            <!-- /.box-body --> 

          </div>

        </div>


        



        <!-- /.box -->



      </section>

      <!-- right col -->

    </div>

    <!-- /.row (main row) -->



  </section>

  <!-- /.content -->

</div>