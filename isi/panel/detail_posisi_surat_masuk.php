<?php
$main = "";

switch($_SESSION["id_level"]){
  case 11 :
  $main = "../index.php?mod=kabidwasnaker&opt=utama";
  break;		
  case 22  :
  $main = "../index.php?mod=kabidlatih&opt=utama";
  break;
  case 4  :
  $main = "../index.php?mod=kabidhubsaker&opt=utama";
  break;
  case 7 :
  $main = "../index.php?mod=kabidpentaker&opt=utama";
  break;
}
?>


<!DOCTYPE html>
<html>
<head>  
<meta name="theme-color" content="#00923F">
<link rel="manifest" href="manifest.json">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>::: DINAS KETENAGAKERJAAN PEMERINTAH KOTA MEDAN :::</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
   <!-- Morris chart -->
   <link rel="stylesheet" href="bower_components/morris.js/morris.css">
   <!-- jvectormap -->
   <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
   <!-- Date Picker -->
   <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
   <!-- Daterange picker -->
   <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
   <!-- bootstrap wysihtml5 - text editor -->
   <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link rel="stylesheet" href="dist/css/custom.css">
</head>
<body class="hold-transition skin-green sidebar-mini">
  <div class="wrapper">

    <?php include 'isi/capekkali/header.php';?>
    <!-- Left side column. contains the logo and sidebar -->
    <?= $_SESSION['menu']?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Dashboard
          <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) --> 
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)--> 
            <!-- /.nav-tabs-custom -->

            <!-- Chat box -->
            <div class="box box-warning"> 
              <div class="box-body">
               
                <input type="button" value="Kembali" onclick="document.location.href='?mod=lacak_surat_masuk';" style="float: right; display: block; font-weight: bold;"/>
                <div class="kelang"></div>
                <div class="panelcontainer" style="width: 100%;">
                  <h3>DETAIL POSISI DISPOSISI</h3>
                  <div class="bodypanel">
                    <table border="0px" cellspacing='0' cellpadding='0' width='100%' class="listingtable">
                      <thead>
                        <tr class="headertable">
                          <th width='40px'>NO.</th>
                          <th width='200px'>ASAL DISPOSISI</th>
                          <th width='200px'>Posisi Disposisi</th>
                          <th width='150px'>Tgl Disposisi</th>
                          <th>Catatan Disposisi</th>
                          <th width='100px'>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        if($_SESSION["id_level"] == 1 || $_SESSION["id_level"] == 2 || $_SESSION["id_level"] == 18 || $_SESSION["id_level"] == 19 || $_SESSION["id_level"] == 24){
                          $sql = "SELECT
                          a.id, b.urutan AS urutan_asal, b.nama_level AS asal, c.urutan AS urutan_tujuan, c.nama_level AS tujuan,
                          d.nama AS nama_asal, e.nama AS nama_tujuan, a.status, a.tgl_disposisi, a.catatan
                          FROM
                          myapp_disptable_suratmasuk a
                          LEFT JOIN myapp_reftable_levelpengguna b ON a.id_level_asal = b.id
                          LEFT JOIN myapp_reftable_levelpengguna c ON a.id_level_tujuan = c.id
                          LEFT JOIN myapp_maintable_pengguna d ON a.id_pengguna_asal = d.id
                          LEFT JOIN myapp_maintable_pengguna e ON a.id_pengguna_tujuan = e.id
                          WHERE
                          a.id_surat_masuk = '" . $_GET["id"] . "' AND a.status < 3
                          GROUP BY
                          a.id";
                        }else{
                          if($_SESSION["id_level"] >= 3 AND $_SESSION["id_level"] <= 6){
                            $tmbh = "( a.id_level_asal='" . $_SESSION["id_level"] . "' OR a.id_level_tujuan='" . $_SESSION["id_level"] . "'";
                            $res_bawahan = mysql_query("SELECT * FROM myapp_reftable_levelpengguna WHERE atasan='" . $_SESSION["id_level"] . "'");
                            while($ds_bawahan = mysql_fetch_array($res_bawahan)){
                              $tmbh .= " OR ";
                              $tmbh .= " a.id_level_tujuan = '" . $ds_bawahan["id"] . "' ";
                            }
                            $tmbh .= ")";
                          }else if($_SESSION["id_level"] >= 7 AND $_SESSION["id_level"] <= 17){
                            $tmbh = "( a.id_level_asal='" . $_SESSION["id_level"] . "' OR a.id_level_tujuan='" . $_SESSION["id_level"] . "'";
                            $res_bawahan = mysql_query("SELECT * FROM myapp_reftable_levelpengguna WHERE atasan='" . $_SESSION["atasan"] . "' AND urutan=4");
                            while($ds_bawahan = mysql_fetch_array($res_bawahan)){
                              $tmbh .= " OR ";
                              $tmbh .= " (a.id_level_asal = '" . $_SESSION["id_level"] . "' AND a.id_level_tujuan = '" . $ds_bawahan["id"] . "') ";
                              $tmbh .= " OR ";
                              $tmbh .= " (a.id_level_asal = '" . $ds_bawahan["id"] . "' AND a.id_level_tujuan = '" . $_SESSION["id_level"] . "') ";
                            }
                            $res_bawahan_langsung = mysql_query("SELECT * FROM myapp_maintable_pengguna WHERE level_kasubbid='" . $_SESSION["id_level"] . "'");
                            while($ds_bawahan_langsung = mysql_fetch_array($res_bawahan_langsung)){
                              $tmbh .= " OR ";
                              $tmbh .= " (a.id_level_asal = '7' AND a.id_pengguna_tujuan = '" . $ds_bawahan_langsung["id"] . "') ";
                            }
                            $tmbh .= ")";
                          }else if($_SESSION["id_level"] >= 20 AND $_SESSION["id_level"] <= 25){
                            $tmbh = " (a.id_pengguna_asal='" . $_SESSION["id_pengguna"] . "' OR a.id_pengguna_tujuan='" . $_SESSION["id_pengguna"] . "')";
                          }
                          $sql = "SELECT
                          a.id, b.urutan AS urutan_asal, b.nama_level AS asal, c.urutan AS urutan_tujuan, c.nama_level AS tujuan,
                          d.nama AS nama_asal, e.nama AS nama_tujuan, a.status, a.tgl_disposisi, a.catatan,
                          a.id_level_tujuan
                          FROM
                          myapp_disptable_suratmasuk a
                          LEFT JOIN myapp_reftable_levelpengguna b ON a.id_level_asal = b.id
                          LEFT JOIN myapp_reftable_levelpengguna c ON a.id_level_tujuan = c.id
                          LEFT JOIN myapp_maintable_pengguna d ON a.id_pengguna_asal = d.id
                          LEFT JOIN myapp_maintable_pengguna e ON a.id_pengguna_tujuan = e.id
                          WHERE
                          a.id_surat_masuk = '" . $_GET["id"] . "' AND a.status < 3 AND " . $tmbh . "
                          GROUP BY
                          a.id";
                        }
            //echo($sql);
                        $res = mysql_query($sql) or die(mysql_error());
                        $ctr = 0;
                        while($ds = mysql_fetch_array($res)){
                          $ctr++;
                          $asal = $ds["asal"];
                          if($ds["urutan_asal"] == 4)
                            $asal .= " (" . $ds["nama_asal"] . ")";
                          
                          $tujuan = $ds["tujuan"];
                          if($ds["urutan_tujuan"] == 4)
                            $tujuan .= " (" . $ds["nama_tujuan"] . ")";
                          
                          $status = "";
                          if($ds["status"] == 1)
                            $status = "<span style='color: red;'>Belum Dibaca</span>";
                          else if($ds["status"])
                            $status = "<span style='color: blue;'>Dibaca</span>";
                          ?>
                          <tr>
                            <td align='center'><?php echo($ctr); ?></td>
                            <td><?php echo($asal); ?></td>
                            <td>
                              <?php echo($tujuan); ?>
                              <?php
                              if(($_SESSION["id_level"] >= 7 && $_SESSION["id_level"] <= 17) && ($ds["id_level_tujuan"] >= 20 && $ds["id_level_tujuan"] <= 25)){
                                ?>
                                <div style="margin-top: 5px; text-align: center;"><a href="javascript:pindah_staff(<?php echo($ds["id"]); ?>);" class="linkkecil">Pindahkan Ke Staff Lain</a></div>
                                <?php
                              }
                              ?>
                            </td>
                            <td><?php echo($ds["tgl_disposisi"]); ?></td>
                            <td><?php echo($ds["catatan"]); ?></td>
                            <td align='center'><?php echo($status); ?></td>
                          </tr>
                          <?php
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
</div><!--
<div class="kelang"></div>
<div class="kelang"></div>
<div class="panelcontainer" style="width: 100%;">
    <h3>DETAIL POSISI PEMROSESAN</h3>
    <div class="bodypanel">
        <table border="0px" cellspacing='0' cellpadding='0' width='100%' class="listingtable">
        <thead>
        <tr class="headertable">
            <th width='40px'>NO.</th>
            <th width='200px'>Asal Pemrosesan</th>
            <th width='200px'>Posisi Pemrosesan</th>
            <th width='150px'>Tgl Diserahkan</th>
            <th>Catatan Pemrosesan</th>
            <th width='100px'>Status</th>
        </tr>
        </thead>
        <tbody>
        <?php 
            $res = mysql_query("SELECT
                                	f.id_surat_keluar, f.id_surat_masuk, b.urutan AS urutan_asal,
                                    b.nama_level AS asal, c.urutan AS urutan_tujuan, c.nama_level AS tujuan, 
                                    d.nama AS nama_asal, e.nama AS nama_tujuan, a.status, a.tgl_disposisi, a.catatan
                                FROM
                                	myapp_disptable_suratkeluar a
                                	LEFT JOIN myapp_reftable_levelpengguna b ON a.id_level_asal = b.id
                                	LEFT JOIN myapp_reftable_levelpengguna c ON a.id_level_tujuan = c.id
                                	LEFT JOIN myapp_maintable_pengguna d ON a.id_pengguna_asal = d.id
                                	LEFT JOIN myapp_maintable_pengguna e ON a.id_pengguna_tujuan = e.id
                                	LEFT JOIN myapp_maintable_balasan f ON a.id_surat_keluar = f.id_surat_keluar
                                WHERE
                                	f.id_surat_masuk = '" . $_GET["id"] . "' AND a.status < 3") or die(mysql_error());
            $ctr = 0;
            while($ds = mysql_fetch_array($res)){
                $ctr++;
                $asal = $ds["asal"];
                if($ds["urutan_asal"] == 4)
                    $asal .= " (" . $ds["nama_asal"] . ")";
                
                $tujuan = $ds["tujuan"];
                if($ds["urutan_tujuan"] == 4)
                    $tujuan .= " (" . $ds["nama_tujuan"] . ")";
                    
                $status = "";
                if($ds["status"] == 1)
                    $status = "<span style='color: red;'>Belum Dibaca</span>";
                else if($ds["status"])
                    $status = "<span style='color: blue;'>Dibaca</span>";
        ?>
            <tr>
                <td align='center'><?php echo($ctr); ?></td>
                <td><?php echo($asal); ?></td>
                <td><?php echo($tujuan); ?></td>
                <td><?php echo($ds["tgl_disposisi"]); ?></td>
                <td><?php echo($ds["catatan"]); ?></td>
                <td align='center'><?php echo($status); ?></td>
            </tr>
        <?php
            }
        ?>
        </tbody>
        </table>
    </div>
</div>
<div class="kelang"></div>
<div class="kelang"></div>
<div class="panelcontainer" style="width: 100%;">
    <h3>SURAT HASIL PEMROSESAN YANG AKAN DIKIRIM (MENUNGGU PROSES PENGIRIMAN)</h3>
    <div class="bodypanel">
        <table border="0px" cellspacing='0' cellpadding='0' width='100%' class="listingtable">
        <thead>
        <tr class="headertable">
            <th width='40px'>NO.</th>
            <th width='200px'>NO. SURAT</th>
            <th width='200px'>No. Nota Dinas</th>
            <th width='200px'>TGL. SURAT</th>
            <th width='200px'>PERIHAL</th>
            <th width='200px'>Tujuan Surat</th>
            <th width='200px'>Penandatangan Surat</th>
        </tr>
        </thead>
        <tbody>
        <?php 
            $res = mysql_query("SELECT
                                	a.*, b.ttd
                                FROM
                                	myapp_maintable_suratkeluar a
                                	LEFT JOIN myapp_reftable_ttd b ON a.id_ttd = b.id
                                	LEFT JOIN myapp_maintable_balasan c ON a.id = c.id_surat_keluar
                                WHERE
                                	c.id_surat_masuk = '" . $_GET["id"] . "' AND a.status = 2") or die(mysql_error());
            $ctr = 0;
            while($ds = mysql_fetch_array($res)){
                $ctr++;
        ?>
            <tr>
                <td align='center'><?php echo($ctr); ?></td>
                <td><?php echo($ds["no_surat"]); ?></td>
                <td><?php echo($ds["no_nodin"]); ?></td>
                <td><?php echo($ds["tgl_surat"]); ?></td>
                <td><?php echo($ds["perihal_surat"]); ?></td>
                <td><?php echo($ds["tujuan_surat"]); ?></td>
                <td><?php echo($ds["ttd"]); ?></td>
            </tr>
        <?php
            }
        ?>
        </tbody>
        </table>
    </div>
</div>
<div class="kelang"></div>
<div class="kelang"></div>
<div class="panelcontainer" style="width: 100%;">
    <h3>SURAT HASIL PEMROSESAN YANG TELAH DIKIRIM</h3>
    <div class="bodypanel">
        <table border="0px" cellspacing='0' cellpadding='0' width='100%' class="listingtable">
        <thead>
        <tr class="headertable">
            <th width='40px'>NO.</th>
            <th width='200px'>NO. SURAT</th>
            <th width='200px'>No. Nota Dinas</th>
            <th width='200px'>TGL. SURAT</th>
            <th width='200px'>PERIHAL</th>
            <th width='200px'>Tujuan Surat</th>
            <th width='200px'>Penandatangan Surat</th>
        </tr>
        </thead>
        <tbody>
        <?php
            $res = mysql_query("SELECT
                                	a.*, b.ttd
                                FROM
                                	myapp_maintable_suratkeluar a
                                	LEFT JOIN myapp_reftable_ttd b ON a.id_ttd = b.id
                                	LEFT JOIN myapp_maintable_balasan c ON a.id = c.id_surat_keluar
                                WHERE
                                	c.id_surat_masuk = '" . $_GET["id"] . "' AND a.status = 3") or die(mysql_error());
            $ctr = 0;
            while($ds = mysql_fetch_array($res)){
                $ctr++;
        ?>
            <tr>
                <td align='center'><?php echo($ctr); ?></td>
                <td><?php echo($ds["no_surat"]); ?></td>
                <td><?php echo($ds["no_nodin"]); ?></td>
                <td><?php echo($ds["tgl_surat"]); ?></td>
                <td><?php echo($ds["perihal_surat"]); ?></td>
                <td><?php echo($ds["tujuan_surat"]); ?></td>
                <td><?php echo($ds["ttd"]); ?></td>
            </tr>
        <?php
            }
        ?>
        </tbody>
        </table>
    </div>
</div>


-->
<div id="dialog_form_disp" title="Pindahkan ke Staff Lain">
  <form name="frm" action="php/pindah_staff.php" method="post">
    <div class="panelcontainer" style="width: 100%;">
      <div class="bodypanel">
        <table border="0px" cellspacing='0' cellpadding='0' width='100%'>
          <input type="hidden" name="id_disposisi" value="" id="id_disposisi" />
          <input type="hidden" name="id_sm" id="id_sm" value="<?php echo($_GET["id"]); ?>" />
          <?php
          $rstaff = mysql_query("SELECT
           b.*
           FROM
           myapp_reftable_levelpengguna a
           LEFT JOIN myapp_maintable_pengguna b ON a.id = b.id_level
           WHERE
           a.atasan = " . $_SESSION["atasan"] . " AND a.urutan = 4");
          while($dstaf = mysql_fetch_array($rstaff)){
            ?>
            <tr>
              <td>
                <input type="radio" name="staff" value="<?php echo($dstaf["id"]); ?>" />
                <?php echo($dstaf["nama"]); ?>
              </td>
            </tr>
            <?php
          }
          ?>
        </table>
        <div class="kelang"></div>
        <table border="0px" cellspacing='0' cellpadding='0' width='100%'>
          <tr>
            <td width='50%'><input type="submit" value='Pindahkan' name="pindah" style="width: 100%;" /></td>
            <td width='50%'>&nbsp;</td>
          </tr>
        </table>
      </div>
    </div>
  </form>
</div>
</div>
<!-- /.chat --> 
</div>
<!-- /.box (chat box) -->

<!-- TO DO List --> 
<!-- /.Left col -->
<!-- right col (We are only adding the ID to make the widgets sortable)--> 
</div>
<!-- /.row (main row) -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include 'isi/capekkali/footer.php';?>
  <div id="dialog_form_disp" class="modal" title="Lanjutkan Surat Ke Kepala Bidang Yang Dituju" role="dialog">
    <form name="frm" action="php/posisi_surat_masuk_kaban.php" method="post">
      <div class="modal-dialog">
       <div class="modal-content">
        <div class="modal-body">
          <table border="0px" cellspacing='0' cellpadding='0' width='100%'>
            <input type="hidden" name="id_surat_masuk" value="" id="id_surat_masuk" />
            <input type="hidden" name="id_disposisi" value="" id="id_disposisi" />
            <?php
            $res_ldb = mysql_query("SELECT * FROM myapp_reftable_levelpengguna WHERE atasan='" . $_SESSION["id_level"] . "' AND id <> 2");
            while($ds_ldb = mysql_fetch_array($res_ldb)){
              ?>
              <tr> 
                <td style="text-transform: capitalize;">
                  <div class="radio">
                    <label><input type="radio" name="id_level_tujuan" value="<?php echo($ds_ldb["id"]); ?>"><?php echo($ds_ldb["nama_level"]); ?></label>
                  </div> 
                </tr>  
                <?php
              }
              ?>
              <tr>
                <td colspan="3">
                        <!--<select onchange="pilihkalimat(1, this.value);"> 
                            <option value="">:: Pilih Kalimat Disposisi ::</option>
                        <?php
                            $rkd = mysql_query("SELECT * FROM myapp_constable_kalimatdisposisi");
                            while($dkd = mysql_fetch_array($rkd)){
                                echo("<option>" . $dkd["kalimat"] . "</option>");
                            }
                        ?>
                      </select>-->
                      <br />
                      <textarea name="catatan" id="catatan_1" placeholder="Pilih Kalimat Disposisi"></textarea>
                    </td>
                  </tr>
                </table> 
                <table border="0px" cellspacing='0' cellpadding='0' width='100%'>
                  <tr>
                    <td width='50%'><input type="submit" value='Didisposisikan' style="width: 100%;" /></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </form>  
        <!-- ./col -->
      </div>
      <!-- Control Sidebar -->
      
      <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
   <div class="control-sidebar-bg"></div>
 </div>
 <!-- ./wrapper -->

 <!-- jQuery 3 -->
 <script src="bower_components/jquery/dist/jquery.min.js"></script>
 <!-- jQuery UI 1.11.4 -->
 <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
 <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
 <script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>


<script type="text/javascript">
  $(document).ready(function(){
    $("#dialog_form_disp").dialog({
      autoOpen: false,
      height: 500,
      width: 700,
      modal: true,
      show: "fade",
      hide: "fade"
    });
  });
  function pindah_staff(id_disposisi){
    $("#id_disposisi").val(id_disposisi);
    $("#dialog_form_disp").dialog("open");
  }
</script>
</body>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
<script>
 if ('serviceWorker' in navigator) {
    console.log("Will the service worker register?");
    navigator.serviceWorker.register('service-worker.js')
      .then(function(reg){
        console.log("Yes, it did.");
     }).catch(function(err) {
        console.log("No it didn't. This happened:", err)
    });
 }
</script>
</html>
  



<!-- ###### -->