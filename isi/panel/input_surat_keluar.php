

<!-- END OF DIALOG -->  

<!DOCTYPE html>

<html>

<head>  
<meta name="theme-color" content="#00923F">
<link rel="manifest" href="manifest.json">

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>::: DINAS KETENAGAKERJAAN PEMERINTAH KOTA MEDAN :::</title>


  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->

  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">

  <!-- Font Awesome -->

  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">

  <!-- Ionicons -->

  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">

  <!-- Theme style -->

  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins

   folder instead of downloading all of them to reduce the load. -->

   <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

   <!-- Morris chart -->

   <link rel="stylesheet" href="bower_components/morris.js/morris.css">

   <!-- jvectormap -->

   <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">

   <!-- Date Picker -->

   <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

   <!-- Daterange picker -->

   <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">

   <!-- bootstrap wysihtml5 - text editor -->

   <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

   <!-- tabel -->

   <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootsnipp-table.css">



   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<![endif]-->



<!-- Google Font -->

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<link rel="stylesheet" href="dist/css/custom.css">
</head>

<body class="hold-transition skin-green sidebar-mini">

  <div class="wrapper">



    <?php include 'isi/capekkali/header.php';?>

    <!-- Left side column. contains the logo and sidebar -->

    <?= $_SESSION['menu']?>



    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">

      <!-- Content Header (Page header) -->

      <section class="content-header">

        <h1>

          INPUT SURAT KELUAR

        </h1>

        <ol class="breadcrumb">

          <li><a href="./"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li>Surat Keluar</li>

          <li class="active">Input Surat Keluar</li>

        </ol>

      </section>
      <section class="content">

        <!-- Small boxes (Stat box) -->

        <div class="row">

          <div class="col-md-12"> 





            <div class="box box-warning"> 
              <div class="box-body">
                <div class="panelcontainer" style="padding: 0 20px;"> 

                  <h4>DAFTAR SURAT-SURAT YANG AKAN DIBALAS<small> 

                    <div class="bodypanel">

                      <table border="0px" cellspacing='0' cellpadding='0' width='100%' class="table table-striped listingtable">

                        <tr class="headertable">

                          <th width='40px'>NO.</th> 

                          <th width='200px'>NO. SURAT</th>

                          <th width='100px'>TGL. SURAT</th>

                          <th width='100px'>TGL. TERIMA</th>

                          <th>PERIHAL</th>

                          <th width='250px'>UNIT PENGIRIM</th>

                          <th width='100px'>HARUS SELESAI</th>

                          <th width='20px'>&nbsp;</th>

                          <th width='20px'>&nbsp;</th>

                          <th width='20px'>&nbsp;</th>

                          <th width='20px'>&nbsp;</th>

                        </tr>

                        <?php

                        $res = mysql_query("SELECT 

                          a.*, b.unit_kerja, c.id AS id_temp

                          FROM 

                          myapp_maintable_suratmasuk a

                          LEFT JOIN myapp_reftable_unitkerja b ON a.id_skpd_pengirim = b.id_unit_kerja

                          LEFT JOIN myapp_sessiontable_balasan c ON (a.id_surat_masuk = c.id_surat_masuk AND c.session_id='" . session_id() . "')

                          WHERE

                          1 AND c.id_surat_masuk IS NOT NULL

                          ORDER BY  

                          id ASC");  

                        $ctr = 0;

                        while($ds = mysql_fetch_array($res)){

                          $ctr++;

                          echo("<tr>");

                          echo("<td align='center'>" . $ctr . "</td>");

                          echo("<td>" . $ds["no_surat"] . "</td>");

                          echo("<td>" . tglindonesia($ds["tgl_surat"]) . "</td>");

                          echo("<td>" . tglindonesia($ds["tgl_terima"]) . "</td>");

                          echo("<td>" . $ds["perihal_surat"] . "</td>");

                          echo("<td>" . $ds["unit_kerja"] . "</td>");

                          if($ds["harus_selesai"] == "0000-00-00")

                            echo("<td>[.:: === ::.]</td>");

                          else

                            echo("<td>" . $ds["harus_selesai"] . "</td>");

                          echo("<td align='center'>");

                          echo("<img src='image/information_32.png' width='18px' class='linkimage' title='Detail Surat Masuk' onclick='lihat_detail_sm(" . $ds["id_surat_masuk"] . ", 0);'>");

                          echo("</td>");

                          echo("<td align='center'>");

                          echo("<img src='image/icon-disposisi.png' width='18px' class='linkimage' title='Daftar catatan disposisi' onclick='lihat_cadis_sm(" . $ds["id_surat_masuk"] . ", 0);'>");

                          echo("</td>");

                          echo("<td align='center'>");

                          echo("<img src='image/Attachment-32.png' width='18px' class='linkimage' title='File yang dilampirkan' onclick='lihat_file_sm(" . $ds["id_surat_masuk"] . ", 0);'>");

                          echo("</td>");

                          echo("<td align='center'>");

                          echo("<img src='image/Delete-32.png' width='18px' class='linkimage' title='Batal balas surat ini' onclick='document.location.href=\"php/batal_temp_balas_surat.php?id=" . $ds["id_temp"] . "\"'>");

                          echo("</td>");

                          echo("</tr>");
                          echo "

                          <tr id='div_if'>

                          <span style='display:none;' id='load_text'></span>

                          <td colspan='11'> <div id='div_cek_".$ds["id_surat_masuk"]."'></div>

                          </tr>

                          ";

                        }

                        ?>

                      </table>

                      <div class="kelang"></div>

                      <input type="button" class="btn btn-success" value="Tambah Surat Untuk Dibalas" onclick="document.location.href='?mod=cari_surat_untuk_dibalas';" /><br>
                      <br><i><font color="red"> *Silahkan kosongkan dan langsung mengisi form dibawah bila ingin membuat surat keluar bukan untuk surat balasan.</font></i></small></h4>

                    </div>


                    <hr>

                    <form style="padding:20px; padding-top: 0px; " name="frm" action="php/input_surat_keluar.php" method="post" id="frm_sk">

                      <div class="panelcontainer" style="width: 100%;"> 

                        <div class="bodypanel">

                          <input type="hidden" name="id_surat_masuk" value="<?php echo($_GET["id"]); ?>" />

                          <input type="hidden" name="id_disposisi" value="<?php echo($_GET["id_disposisi"]); ?>" />



                          <?php

                          if(isset($_GET["err"])){

                            ?>
                            <span class="err_msg"><?php echo($_GET["err"]) ?></span>


                            <?php

                          }

                          ?>


                          <div class="form-group col-md-6">

                            <label>Nomor Surat</label>

                            <input type="text" name="" value="Diisi oleh Kasubbag Umum Pada Proses Pengiriman Surat Keluar" class="form-control" disabled=""> 

                          </div>

                          <div class="form-group col-md-6">

                            <label>Tanggal Surat</label>

                            <input type="text" name="" value="Diisi oleh Kasubbag Umum Pada Proses Pengiriman Surat Keluar" class="form-control" disabled=""> 

                          </div> 

                          <div class="form-group col-md-6">

                            <label>Perihal</label>

                            <input type="text" name="perihal_surat" class="form-control" required>

                          </div> 

                          <div class="form-group col-md-6">

                            <label>Tujuan</label>

                            <input type="text" name="tujuan_surat" class="form-control" required>

                          </div> 
                          <div class="form-group col-md-6">

                            <label>Alamat Tujuan</label>

                            <input type="text" name="alamat_tujuan" class="form-control" required>

                          </div>

                          <div class="form-group col-md-6">

                            <label>Judul Surat</label>

                            <input type="text" name="judul_surat" class="form-control" required>

                          </div> 
                          <div class="form-group col-md-6">

                            <label>Deskripsi Surat</label>

                            <input type="text" name="deskripsi_surat" class="form-control" required>

                          </div> 

                          <div class="form-group col-md-6">

                            <label>Catatan Tambahan</label>

                            <textarea type="text" name="catatan" class="form-control" required></textarea>

                          </div> 
                          <div class="form-group col-md-6">

                            <label>Pilih Jenis Instansi Tujuan</label>

                            <select class="form-control" name="" onchange="loadtingkatan(this.value);">

                              <option value="0" hidden>[.. Pilih Jenis Instansi Tujuan ..]</option>
                              <option value='1'>Instansi Pemerintahan</option>
                              <option value='2'>Instansi NON Pemerintahan</option>


                            </select>

                          </div>

                          <div class="form-group col-md-6">

                            <label>Pilih Bentuk / Tingkatan Instansi Tujuan</label>

                            <select class="form-control" name="" id="tingkatan" onchange="loadinstansi(this.value);">

                              <option value="0" hidden>[.. Pilih Bentuk / Tingkatan Instansi Tujuan ..]</option> 

                            </select>

                          </div>


                          <div class="form-group col-md-6">

                            <label>Pilih Instansi Tujuan</label>

                            <select class="form-control" name="id_skpd_tujuan" id="id_skpd_pengirim">

                              <option value="0" hidden>[.. Pilih Instansi Tujuan ..]</option>
                            </select>

                          </div>



                          <div class="form-group col-md-6">

                            <label>Masalah</label>

                            <select class="form-control"  name="id_masalah" onchange="loadsubmasalah(this.value);">

                              <option value="0" hidden>[.. Pilih Masalah ..]</option>

                              <?php

                              $res_masalah= mysql_query("SELECT * FROM myapp_reftable_masalah ORDER BY kode_masalah ASC");

                              while($ds_masalah = mysql_fetch_array($res_masalah)){

                                echo("<option value='" . $ds_masalah["id_masalah"] . "'>(" . $ds_masalah["kode_masalah"] . ") " . $ds_masalah["masalah"] . "</option>");

                              }

                              ?>

                            </select>

                          </div> 

                          <div class="form-group col-md-6">
                            <label>Sub Masalah</label>

                            <select class="form-control" name="id_jenis_surat" id="id_jenis_surat">

                              <option value="0">[.. Pilih Sub Masalah ..]</option>

                            </select>

                          </div>
                           
                          <div class="form-group col-md-6">

                            <label>Penanda Tangan</label>

                            <select name="id_ttd" class="form-control">

                             <option value="1">Kepala Dinas</option> 
                           </select>

                         </div> 



                         <div class="form-group col-md-6">

                           <input type="submit" value='Simpan' class="btn btn-success"/> 
                           <input type="reset" value='Reset' class="btn btn-danger" />

                         </div>



                       </div>

                     </div>

                   </form>
                 </div>
               </div>

               <!-- /.chat --> 

             </div>
           </div>





           <!-- DIALOG -->

           <div id="dialog_form_disp" class="modal" title="Lanjutkan Surat Ke Kepala Bidang Yang Dituju" role="dialog">

            <form name="frm" action="php/posisi_surat_masuk_kaban.php" method="post">

              <div class="modal-dialog">

                <div class="modal-content">

                  <div class="modal-body">

                    <table border="0px" cellspacing='0' cellpadding='0' width='100%'>

                      <input type="hidden" name="id_surat_masuk" value="" id="id_surat_masuk" />

                      <input type="hidden" name="id_disposisi" value="" id="id_disposisi" />

                      <?php

                      $res_ldb = mysql_query("SELECT * FROM myapp_reftable_levelpengguna WHERE atasan='" . $_SESSION["id_level"] . "' AND id <> 2");

                      while($ds_ldb = mysql_fetch_array($res_ldb)){

                        ?>

                        <tr> 

                          <td style="text-transform: capitalize;">

                            <div class="radio">

                              <label><input type="radio" name="id_level_tujuan" value="<?php echo($ds_ldb["id"]); ?>"><?php echo($ds_ldb["nama_level"]); ?></label>

                            </div> 

                          </tr>  

                          <?php

                        }

                        ?>

                        <tr>

                          <td colspan="3">

                        <!--<select onchange="pilihkalimat(1, this.value);"> 

                            <option value="">:: Pilih Kalimat Disposisi ::</option>

                        <?php

                            $rkd = mysql_query("SELECT * FROM myapp_constable_kalimatdisposisi");

                            while($dkd = mysql_fetch_array($rkd)){

                                echo("<option>" . $dkd["kalimat"] . "</option>");

                            }

                        ?>

                      </select>-->

                      <br />

                      <textarea name="catatan" id="catatan_1" placeholder="Pilih Kalimat Disposisi"></textarea>

                    </td>

                  </tr>

                </table> 

                <table border="0px" cellspacing='0' cellpadding='0' width='100%'>

                  <tr>

                    <td width='50%'><input type="submit" value='Kirim Disposisi' style="width: 100%;" /></td>

                  </tr>

                </table>

              </div>

            </div>

          </div>

        </form>  

        <!-- ./col -->

      </div>

      

      <!-- /.row -->

      <!-- Main row --> 

      <!-- /.row (main row) -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

  <?php include 'isi/capekkali/footer.php';?>



    <!-- Control Sidebar -->

    

    <!-- /.control-sidebar -->

  <!-- Add the sidebar's background. This div must be placed

   immediately after the control sidebar -->

   <div class="control-sidebar-bg"></div>

 </div>

 <!-- ./wrapper -->



 <!-- jQuery 3 -->

 <script src="bower_components/jquery/dist/jquery.min.js"></script>

 <!-- jQuery UI 1.11.4 -->

 <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>

 <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->



 <script>

  $.widget.bridge('uibutton', $.ui.button);

</script>

<!-- Bootstrap 3.3.7 -->

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Morris.js charts -->

<script src="bower_components/raphael/raphael.min.js"></script>

<script src="bower_components/morris.js/morris.min.js"></script>

<!-- Sparkline -->

<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>

<!-- jvectormap -->

<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- jQuery Knob Chart -->

<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>

<!-- daterangepicker -->

<script src="bower_components/moment/min/moment.min.js"></script>

<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- datepicker -->

<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<!-- Bootstrap WYSIHTML5 -->

<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

<!-- Slimscroll -->

<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->

<script src="bower_components/fastclick/lib/fastclick.js"></script>

<!-- AdminLTE App -->

<script src="dist/js/adminlte.min.js"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<script src="dist/js/pages/dashboard.js"></script>

<!-- AdminLTE for demo purposes -->

<script src="dist/js/demo.js"></script>



<script type="text/javascript">

  $(document).ready(function(){

    $("#tgl_surat").datepicker({

      dateFormat: "yy-mm-dd",

      changeMonth: true,

      changeYear: true

    });

    $("#tgl_terima").datepicker({

      dateFormat: "yy-mm-dd",

      changeMonth: true,

      changeYear: true

    });

    $("#harus_selesai").datepicker({

      dateFormat: "yy-mm-dd",

      changeMonth: true,

      changeYear: true

    });

    

  });




  function loadsubmasalah(id_masalah){

    //alert(id_masalah);

    $("#id_jenis_surat").html("<option value='0'>[.. Pilih Sub Masalah ..]</option>");

    $.ajax({

      type: "GET",

      url: "ajax/submasalah.php",

      data: "id_masalah=" + id_masalah,

      success: function(r){

            //alert(r);

            $("#id_jenis_surat").append(r);

        }

    });

}

function loadtingkatan(tingkatan){

    //alert(tingkatan);

    $("#tingkatan").html("<option value='0'>[.. Pilih Tingkatan ..]</option>");

    $.ajax({

      type: "GET",

      url: "ajax/tingkatan.php",

      data: "tingkatan=" + tingkatan,

      success: function(r){

            //alert(r);

            $("#tingkatan").append(r);

        }

    });

}

function loadinstansi(id_skpd_pengirim){

    //alert(id_skpd_pengirim);

    $("#id_skpd_pengirim").html("<option value='0'>[.. Pilih Tingkatan ..]</option>");

    $.ajax({

      type: "GET",

      url: "ajax/instansi.php",

      data: "id_skpd_pengirim=" + id_skpd_pengirim,

      success: function(r){

             //alert(r);

             $("#id_skpd_pengirim").append(r);

         }

     });

}


 

</script>
<script type="text/javascript" src="./libraries/main.js"></script>

</body>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
<script>
 if ('serviceWorker' in navigator) {
    console.log("Will the service worker register?");
    navigator.serviceWorker.register('service-worker.js')
      .then(function(reg){
        console.log("Yes, it did.");
     }).catch(function(err) {
        console.log("No it didn't. This happened:", err)
    });
 }
</script>
</html>
















