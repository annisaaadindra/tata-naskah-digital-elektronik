
<!-- END OF DIALOG -->





<!DOCTYPE html>
<html>
<head>  
<meta name="theme-color" content="#00923F">
<link rel="manifest" href="manifest.json">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>::: DINAS KETENAGAKERJAAN PEMERINTAH KOTA MEDAN :::</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">

  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
   <!-- Morris chart -->
   <link rel="stylesheet" href="bower_components/morris.js/morris.css">
   <!-- jvectormap -->
   <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
   <!-- Date Picker -->
   <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
   <!-- Daterange picker -->
   <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
   <!-- bootstrap wysihtml5 - text editor -->
   <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link rel="stylesheet" href="dist/css/custom.css">
</head>
<body class="hold-transition skin-green sidebar-mini">
  <div class="wrapper">

    <?php include 'isi/capekkali/header.php';?>
    <!-- Left side column. contains the logo and sidebar -->
    <?= $_SESSION['menu']?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Detail Posisi Surat Masuk
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-md-12">
            <div class="panelcontainer" style="width: 100%;"> 
              <div class="bodypanel"> 
                
   <!--input type="button" value="+" style="float: right; display: block; font-weight: bold;" id="jebol" /><br/><br/>
   <fieldset id='bodyFilter'>
        <legend><h3><small>FILTER DATA PENCARIAN</small></h3></legend>
		
		<form name="frm" action="?mod=lacak_surat_masuk" method="POST">
            <table border="0px" cellspacing='0' cellpadding='0' width='100%'>
                <tr>
                    <td width='20%'>Nomor Surat</td>
                    <td width='10px'>:</td>
                    <td><input type="text" name="no_surat" value="<?=isset($_POST["no_surat"]) ? $_POST["no_surat"] : "" ; ?>" /></td>
                </tr>
                <tr>
                    <td width='20%'>Tanggal Surat</td>
                    <td width='10px'>:</td>
                    <td>
                        <input type="text" name="tgl_surat_dari" id="tgl_surat_dari" class="ufilter" value="<?=isset($_POST["tgl_surat_dari"]) ? $_POST["tgl_surat_dari"] : ""; ?>" />
                        S/D
                        <input type="text" name="tgl_surat_sampai" id="tgl_surat_sampai" class="ufilter" value="<?=isset($_POST["tgl_surat_sampai"]) ? $_POST["tgl_surat_sampai"] : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <td width='20%'>Tanggal Terima</td>
                    <td width='10px'>:</td>
                    <td>
                        <input type="text" name="tgl_terima_dari" id="tgl_terima_dari" class="ufilter" value="<?=isset($_POST["tgl_terima_dari"]) ? $_POST["tgl_terima_dari"] : "" ; ?>" />
                        S/D
                        <input type="text" name="tgl_terima_sampai" id="tgl_terima_sampai" class="ufilter" value="<?=isset($_POST["tgl_terima_sampai"]) ? $_POST["tgl_terima_sampai"] : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <td width='20%'>Perihal</td>
                    <td width='10px'>:</td>
                    <td><input type="text" name="perihal_surat" value="<?=isset($_POST["perihal_surat"]) ? $_POST["perihal_surat"] : ""; ?>" /></td>
                </tr>
                <tr>
                    <td width='20%'>Deskripsi Surat</td>
                    <td width='10px'>:</td>
                    <td><input type="text" name="deskripsi_surat" value="<?=isset($_POST["deskripsi_surat"]) ? $_POST["deskripsi_surat"] : ""; ?>" /></td>
                </tr>
                <tr>
                    <td width='20%'>SKPD / Unit Pengirim</td>
                    <td width='10px'>:</td>
                    <td>
                        <select name="id_skpd_pengirim">
                            <option value="0">[.. Pilih SKPD Pengirim ..]</option>
                        <?php
                            $res_skpd = mysql_query("SELECT * FROM myapp_reftable_unitkerja ORDER BY unit_kerja ASC");
                            while($ds_skpd = mysql_fetch_array($res_skpd)){
                                if($ds_skpd["id_unit_kerja"] == $_POST["id_skpd_pengirim"])
                                    echo("<option selected='selected' value='" . $ds_skpd["id_unit_kerja"] . "'>" . $ds_skpd["unit_kerja"] . "</option>");
                                else
                                    echo("<option value='" . $ds_skpd["id_unit_kerja"] . "'>" . $ds_skpd["unit_kerja"] . "</option>");
                            }
                        ?>
                        </select>
                    </td>
                </tr>
            </table><br/>
            <table border="0px" cellspacing='0' cellpadding='0' width='40%'>
                <tr>
                    <td width='50%'><input type="submit" value='Filter' style="width: 100%;" /></td>
                    <td width='50%'><input type="reset" value='Reset' style="width: 100%;" /></td>
                </tr>
            </table>
		</form>
	</fieldset>	<br/><br/--> 
  <div class="box box-warning"> 
    <div class="box-body">
      <fieldset>
        <legend><h3>CARI SURAT MASUK</h3></legend>
        <table id="example2" class="listingtable table table-bordered table-striped">
          <tfoot>
            <tr>
              <th width='40px'>NO.</th>
              <th width='200px'>NO. SURAT</th>
              <th width='100px'>TGL. SURAT</th>
              <th width='100px'>TGL. TERIMA</th>
              <th>PERIHAL</th>
              <th width='250px'>UNIT PENGIRIM</th>
              <th width='100px'>HARUS SELESAI</th>
              
              <th style="display: none;" width='20px'>&nbsp;</th>
              <th style="display: none;" width='20px'>&nbsp;</th>
              <th style="display: none;" width='20px'>&nbsp;</th>
              <th style="display: none;" width='20px'>&nbsp;</th>
            </tr>
          </tfoot>
          <thead>
            <tr>
              <th width='40px'>NO.</th>
              <th width='200px'>NO. SURAT</th>
              <th width='100px'>TGL. SURAT</th>
              <th width='100px'>TGL. TERIMA</th>
              <th>PERIHAL</th>
              <th width='250px'>UNIT PENGIRIM</th>
              <th width='100px'>HARUS SELESAI</th>
              
              <th width='20px'>&nbsp;</th>
              <th width='20px'>&nbsp;</th>
              <th width='20px'>&nbsp;</th>
              <th width='20px'>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $whr = "";
            $sql = "";
            if($_SESSION["id_level"]==1 || $_SESSION["id_level"]==2 || $_SESSION["id_level"]==18){
              if(isset($_POST["no_surat"]) && $_POST["no_surat"] <> "")
                $whr .= " AND a.no_surat LIKE '%" . $_POST["no_surat"] . "%'";
              if(isset($_POST["tgl_surat_dari"]) && isset($_POST["tgl_surat_sampai"]) && $_POST["tgl_surat_dari"] <> "" && $_POST["tgl_surat_sampai"] <> "")
                $whr .= " AND a.tgl_surat BETWEEN '" . $_POST["tgl_surat_dari"] . "' AND '" . $_POST["tgl_surat_sampai"] . "'";
              if(isset( $_POST["tgl_terima_dari"]) && isset($_POST["tgl_terima_sampai"]) && $_POST["tgl_terima_dari"] <> "" && $_POST["tgl_terima_sampai"] <> "")
                $whr .= " AND a.tgl_terima BETWEEN '" . $_POST["tgl_terima_dari"] . "' AND '" . $_POST["tgl_terima_sampai"] . "'";
              if(isset( $_POST["perihal_surat"]) && $_POST["perihal_surat"] <> "")
                $whr .= " AND a.perihal_surat LIKE '%" . $_POST["perihal_surat"] . "%'";
              if(isset($_POST["deskripsi_surat"]) && $_POST["deskripsi_surat"] <> "")
                $whr .= " AND a.deskripsi_surat LIKE '%" . $_POST["deskripsi_surat"] . "%'";
              if(isset($_POST["id_skpd_pengirim"]) && $_POST["id_skpd_pengirim"] <> 0)
                $whr .= " AND a.id_skpd_pengirim = '" . $_POST["id_skpd_pengirim"] . "'";
              
              
              $sql = "SELECT 
              a.*, b.unit_kerja
              FROM 
              myapp_maintable_suratmasuk a
              LEFT JOIN myapp_reftable_unitkerja b ON a.id_skpd_pengirim = b.id_unit_kerja
              WHERE
              1 " . $whr . "
              GROUP BY
              a.id
              ORDER BY 
              id DESC";
            }else{
              if(isset($_POST["no_surat"]) && $_POST["no_surat"] <> "")
                $whr .= " AND a.no_surat LIKE '%" . $_POST["no_surat"] . "%'";
              if(isset($_POST["tgl_surat_dari"]) && isset($_POST["tgl_surat_sampai"]) && $_POST["tgl_surat_dari"] <> "" && $_POST["tgl_surat_sampai"] <> "")
                $whr .= " AND a.tgl_surat BETWEEN '" . $_POST["tgl_surat_dari"] . "' AND '" . $_POST["tgl_surat_sampai"] . "'";
              if(isset( $_POST["tgl_terima_dari"]) && isset($_POST["tgl_terima_sampai"]) && $_POST["tgl_terima_dari"] <> "" && $_POST["tgl_terima_sampai"] <> "")
                $whr .= " AND a.tgl_terima BETWEEN '" . $_POST["tgl_terima_dari"] . "' AND '" . $_POST["tgl_terima_sampai"] . "'";
              if(isset( $_POST["perihal_surat"]) && $_POST["perihal_surat"] <> "")
                $whr .= " AND a.perihal_surat LIKE '%" . $_POST["perihal_surat"] . "%'";
              if(isset($_POST["deskripsi_surat"]) && $_POST["deskripsi_surat"] <> "")
                $whr .= " AND a.deskripsi_surat LIKE '%" . $_POST["deskripsi_surat"] . "%'";
              if(isset($_POST["id_skpd_pengirim"]) && $_POST["id_skpd_pengirim"] <> 0)
                $whr .= " AND a.id_skpd_pengirim = '" . $_POST["id_skpd_pengirim"] . "'";
              
            // PERTAMA CARI URUTANNYA
              $ds_urutan = mysql_fetch_array(mysql_query("SELECT * FROM myapp_reftable_levelpengguna WHERE id='" . $_SESSION["id_level"] . "'"));
              $urutan = $ds_urutan["urutan"];
              
              $tmbh = " (c.id_level_tujuan = '" . $_SESSION["id_level"] . "' ";
              if($urutan == 2){
                $res_bawahan = mysql_query("SELECT * FROM myapp_reftable_levelpengguna WHERE atasan='" . $_SESSION["id_level"] . "'");
                while($ds_bawahan = mysql_fetch_array($res_bawahan)){
                  $tmbh .= " OR ";
                  $tmbh .= " c.id_level_tujuan = '" . $ds_bawahan["id"] . "' ";
                  
                }
              }else if($urutan == 3){
                $res_bawahan = mysql_query("SELECT * FROM myapp_reftable_levelpengguna WHERE atasan='" . $_SESSION["atasan"] . "' AND urutan=4");
                while($ds_bawahan = mysql_fetch_array($res_bawahan)){
                  $tmbh .= " OR ";
                  $tmbh .= " (c.id_level_asal = '" . $_SESSION["id_level"] . "' AND c.id_level_tujuan = '" . $ds_bawahan["id"] . "') ";
                  $tmbh .= " OR ";
                  $tmbh .= " (c.id_level_asal = '" . $ds_bawahan["id"] . "' AND c.id_level_tujuan = '" . $_SESSION["id_level"] . "') ";
                }
                $res_bawahan_langsung = mysql_query("SELECT * FROM myapp_maintable_pengguna WHERE level_kasubbid='" . $_SESSION["id_level"] . "'");
                while($ds_bawahan_langsung = mysql_fetch_array($res_bawahan_langsung)){
                  $tmbh .= " OR ";
                  $tmbh .= " (c.id_level_asal = '7' AND c.id_pengguna_tujuan = '" . $ds_bawahan_langsung["id"] . "') ";
                }
              }
              $tmbh .= " ) ";
              $sql = "SELECT 
              a.*, b.unit_kerja
              FROM 
              myapp_maintable_suratmasuk a
              LEFT JOIN myapp_reftable_unitkerja b ON a.id_skpd_pengirim = b.id_unit_kerja
              LEFT JOIN myapp_disptable_suratmasuk c ON a.id_surat_masuk = c.id_surat_masuk
              WHERE
              1 AND " . $tmbh . $whr . "
              GROUP BY
              a.id_surat_masuk
              ORDER BY 
              id DESC";
            }
        //echo($sql);
            $res = mysql_query($sql) or die(mysql_error()); 
            $ctr = 0;
            while($ds = mysql_fetch_array($res)){
              $ctr++;
              echo("<tr>");
              echo("<td align='center'>" . $ctr . "</td>");
              echo("<td>" . $ds["no_surat"] . "</td>");
              echo("<td>" . tglindonesia($ds["tgl_surat"]) . "</td>");
              echo("<td>" . tglindonesia($ds["tgl_terima"]) . "</td>");
              echo("<td>" . $ds["perihal_surat"] . "</td>");
              echo("<td>" . $ds["unit_kerja"] . "</td>");
              if($ds["harus_selesai"] == "0000-00-00")
                echo("<td>[.:: === ::.]</td>");
              else
                echo("<td>" . $ds["harus_selesai"] . "</td>");
              echo("<td align='center'>");
              echo("<img src='image/information_32.png' width='18px' class='linkimage' title='Detail Surat Masuk' onclick='lihat_detail_sm(" . $ds["id_surat_masuk"] . ", 0);'>");
              echo("</td>");
              echo("<td align='center'>");
              echo("<img src='image/icon-disposisi.png' width='18px' class='linkimage' title='Daftar catatan disposisi' onclick='lihat_cadis_sm(" . $ds["id_surat_masuk"] . ", 0);'>");
              echo("</td>");
              echo("<td align='center'>");
              echo("<img src='image/Attachment-32.png' width='18px' class='linkimage' title='File yang dilampirkan' onclick='lihat_file_sm(" . $ds["id_surat_masuk"] . ", 0);'>");
              echo("</td>");
              echo("<td align='center'>");
              echo("<img src='image/Application-View-Detail-32.png' width='18px' class='linkimage' title='Detail Posisi Surat Masuk' onclick='document.location.href=\"?mod=detail_posisi_surat_masuk&id=" . $ds["id_surat_masuk"] . "\"'>");
              echo("</td>");
              echo("</tr>");
              
              echo "
              <tr id='div_if'>
              <span style='display:none;' id='load_text'></span>
              <td colspan='11'> <div id='div_cek_".$ds["id_surat_masuk"]."'></div></td>
              <td style='display: none;'></td>
              <td style='display: none;'></td>
              <td style='display: none;'></td>
              <td style='display: none;'></td>
              <td style='display: none;'></td>
              <td style='display: none;'></td>
              <td style='display: none;'></td>
              <td style='display: none;'></td>
              <td style='display: none;'></td>
              <td style='display: none;'></td>
              </tr>
              ";	
            }
            ?>
          </tbody>
        </table>
      </fieldset>
    </div>
  </div>
</div>
</div>
<!-- DIALOG -->
<div id="dialog_form_disp" class="modal" title="Lanjutkan Surat Ke Kepala Bidang Yang Dituju" role="dialog">
  <form name="frm" action="php/posisi_surat_masuk_kaban.php" method="post">
    <div class="modal-dialog">
     <div class="modal-content">
      <div class="modal-body">
        <table border="0px" cellspacing='0' cellpadding='0' width='100%'>
          <input type="hidden" name="id_surat_masuk" value="" id="id_surat_masuk" />
          <input type="hidden" name="id_disposisi" value="" id="id_disposisi" />
          <?php
          $res_ldb = mysql_query("SELECT * FROM myapp_reftable_levelpengguna WHERE atasan='" . $_SESSION["id_level"] . "' AND id <> 2");
          while($ds_ldb = mysql_fetch_array($res_ldb)){
            ?>
            <tr>
              <td width='5px'><input type="checkbox" name="id_level_tujuan_<?php echo($ds_ldb["id"]); ?>" /></td>
              <td style="text-transform: capitalize;"><?php echo($ds_ldb["nama_level"]); ?></td>
            </tr>  
            <?php
          }
          ?>
          <tr>
            <td colspan="3">
                        <!--<select onchange="pilihkalimat(1, this.value);">
                            <option value="">:: Pilih Kalimat Disposisi ::</option>
                        <?php
                            $rkd = mysql_query("SELECT * FROM myapp_constable_kalimatdisposisi");
                            while($dkd = mysql_fetch_array($rkd)){
                                echo("<option>" . $dkd["kalimat"] . "</option>");
                            }
                        ?>
                      </select>-->
                      <br />
                      <textarea name="catatan" id="catatan_1" placeholder="Pilih Kalimat Disposisi"></textarea>
                    </td>
                  </tr>
                </table> 
                <table border="0px" cellspacing='0' cellpadding='0' width='100%'>
                  <tr>
                    <td width='50%'><input type="submit" value='Didisposisikan' style="width: 100%;" /></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </form>  
        <!-- ./col -->
      </div>
      
      <!-- /.row -->
      <!-- Main row --> 
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include 'isi/capekkali/footer.php';?>

    <!-- Control Sidebar -->
    
    <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
   <div class="control-sidebar-bg"></div>
 </div>
 <!-- ./wrapper -->

 <!-- jQuery 3 -->
 <script src="bower_components/jquery/dist/jquery.min.js"></script>
 <!-- jQuery UI 1.11.4 -->
 <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
 <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

 <script type="text/javascript">
  $(document).ready(function(){
    $("#dialog_form_disp").dialog({
      autoOpen: false,
      height: "auto",
      width: 700,
      modal: true,
      show: "fade",
      hide: "fade"
    });
  });
  function munculDisposisi(id_surat_masuk, id_disposisi){
    $("#id_surat_masuk").val(id_surat_masuk);
    $("#id_disposisi").val(id_disposisi);
    $("#dialog_form_disp").dialog("open");
  }

  $(document).ready(function(){
    $("#expand").click(function(){
      $("#bodyfilter").slideToggle(500);
    });
    
    $("#tgl_surat_dari").datepicker({
      dateFormat: "yy-mm-dd",
      changeMonth: true,
      changeYear: true
    });
    $("#tgl_terima_dari").datepicker({
      dateFormat: "yy-mm-dd",
      changeMonth: true,
      changeYear: true
    });
    
    $("#tgl_surat_sampai").datepicker({
      dateFormat: "yy-mm-dd",
      changeMonth: true,
      changeYear: true
    });
    $("#tgl_terima_sampai").datepicker({
      dateFormat: "yy-mm-dd",
      changeMonth: true,
      changeYear: true
    });
  });
  function pilihkalimat(id, kalimat){
    $("#catatan_" + id).val(kalimat);
  }
</script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- dtt -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    
   $("#jebol").click(function(){
    alert("test");
  });
 });
  function disposisi(id_surat_masuk, id_disposisi){
    var t = document.getElementById("div_cek_"+id_surat);
    if(t.innerHTML != ''){
     t.innerHTML = '';
   }else
   t.innerHTML='<iframe src="./isi/panel/modal/dialog_lacak_sm.php?id_surat_masuk='+id_surat_masuk+'&id_disposisi='+id_disposisi+'" width="100%" style="height:1300px" frameborder="0" ' + 
   'scrolling="no" id="iframe_detail"></iframe>';	
 }

</script>
<script type="text/javascript" src="./libraries/main.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : false
    })
  })
  $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#example2 tfoot th').each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Cari '+title+'" />' );
    } );
    
    // DataTable
    var table = $('#example2').DataTable();
    
    // Apply the search
    table.columns().every( function () {
      var that = this;
      
      $( 'input', this.footer() ).on( 'keyup change', function () {
        if ( that.search() !== this.value ) {
          that
          .search( this.value )
          .draw();
        }
      } );
    } );
  } );
</script>
</body>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
<script>
 if ('serviceWorker' in navigator) {
    console.log("Will the service worker register?");
    navigator.serviceWorker.register('service-worker.js')
      .then(function(reg){
        console.log("Yes, it did.");
     }).catch(function(err) {
        console.log("No it didn't. This happened:", err)
    });
 }
</script>
</html>






