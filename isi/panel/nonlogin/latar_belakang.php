 


























    <link href="css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/animate.css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/jquery.bxslider.css">
  <link rel="stylesheet" type="text/css" href="css/normalize.css" />
  <link rel="stylesheet" type="text/css" href="css/demo.css" />
  <link rel="stylesheet" type="text/css" href="css/set1.css" />
  <link href="css/overwrite.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse.collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
    
        <a class="navbar-brand" href="beranda.php"><span><img src="dist/img/logo.png" height="40px">   TNDE</span></a>
      </div>
      <div class="navbar-collapse collapse">              
        <div class="menu">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation"><a href="./">Beranda</a></li>
            <li role="presentation"><a href="./?mod=berita_dan_informasi">Berita & Informasi</a></li>
            <li role="presentation"><a href="?mod=fdownload">Download</a></li>
            <li role="presentation" class="active"><a href="./?mod=latar_belakang">Latar Belakang</a></li>
            <li role="presentation"><a href="?mod=login">Login</a></li>           
          </ul>
        </div>
      </div>      
    </div>
  </nav>
  
  <div class="container">
        <div class="row">
            <div class="service">
                <div class="col-md-6 col-md-offset-3">
                    <div class="text-center">
                        <h2>Latar Belakang</h2> 
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
  
  <div class="container">
    <div class="row">
       <div id='latar' style='padding-top:10px;'>
                <br>
  <fieldset style='width:90%; margin-left:20px; padding-left:20px;min-height:200px; margin-bottom:20px;'> 
    <br />
     
          
      <table>
        
        <tr>
          <td style="font-size:14px">
      <?php
        $hasil_latar = mysql_query("SELECT latar_belakang  from myapp_statictable_latarbelakang where id_latar_belakang = 1");
        $baris_latar = mysql_fetch_array($hasil_latar);
        echo $baris_latar["latar_belakang"];
      ?>
          </td>
        </tr>
      </table>
</fieldset>
</div>
    </div>
  </div>  
    
  <footer>
    <div class="inner-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4 f-about">
            <a href="beranda.php"><h1><span></span>TNDE</h1></a>
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit Cras suscipit arcu libero
            vestibulum volutpat libero sollicitudin vitae Curabitur ac aliquam  consectetur adipiscing elit Cras suscipit arcu libero
            </p>
          </div>
          <div class="col-md-4 l-posts">
            <h3 class="widgetheading">Berita Terakhir</h3>
            <ul>
              <li><a href="#">This is awesome post title</a></li>
              <li><a href="#">Awesome features are awesome</a></li>
              <li><a href="#">Create your own awesome website</a></li>
              <li><a href="#">Wow, this is fourth post title</a></li>
            </ul>
          </div>
          <div class="col-md-4 f-contact">
            <h3 class="widgetheading">Kontak Kami</h3>
            <a href="#"><p><i class="fa fa-envelope"></i> example@gmail.com</p></a>
            <p><i class="fa fa-phone"></i>  +345 578 59 45 416</p>
            <p><i class="fa fa-home"></i> Enno inc  |  PO Box 23456 
              Little Lonsdale St, New York <br>
              Victoria 8011 USA</p>
          </div>
        </div>
      </div>
    </div>
    
    
    <div class="last-div">
      <div class="container">
        <div class="row">
          <div class="copyright">
            © 2017 TNDE</a>
          </div>  
                    <!-- 
                        All links in the footer should remain intact. 
                        Licenseing information is available at: http://bootstraptaste.com/license/
                        You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=eNno
                    -->       
        </div>
      </div>
      <!--div class="container">
        <div class="row">
          <ul class="social-network">
            <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook fa-1x"></i></a></li>
            <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter fa-1x"></i></a></li>
            <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin fa-1x"></i></a></li>
            <li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest fa-1x"></i></a></li>
            <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus fa-1x"></i></a></li>
          </ul>
        </div>
      </div-->
      
      <a href="" class="scrollup"><i class="fa fa-chevron-up"></i></a>  
        
      
    </div>  
  </footer>
  
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-2.1.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.isotope.min.js"></script>
  <script src="js/jquery.bxslider.min.js"></script>
  <script type="text/javascript" src="js/fliplightbox.min.js"></script>
  <script src="js/functions.js"></script> 
  <script type="text/javascript">$('.portfolio').flipLightBox()</script>
  </body>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
<script>
 if ('serviceWorker' in navigator) {
    console.log("Will the service worker register?");
    navigator.serviceWorker.register('service-worker.js')
      .then(function(reg){
        console.log("Yes, it did.");
     }).catch(function(err) {
        console.log("No it didn't. This happened:", err)
    });
 }
</script>
</html>
