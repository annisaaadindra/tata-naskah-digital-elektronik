<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>  
<meta name="theme-color" content="#00923F">
<link rel="manifest" href="manifest.json">
	<title>SISTEM INFORMASI MANAJEMEN DINAS SOSIAL</title>
    <link type="image/x-icon" href="./image/icon.png" rel="shortcut icon"></link>
	<link rel='stylesheet' href='./css/menu.css' type='text/css' />
	<link rel='stylesheet' href='./css/style.css' type='text/css' />
	<link rel='stylesheet' href='./css/tabel.css' type='text/css' />
	<link rel='stylesheet' href='./css/button.css' type='text/css' />    
</head>
<body>

<?php
	error_reporting(E_ALL ^ E_NOTICE);
		include "./libraries/dinsos.php";
?>

</div>
</body>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
<script>
 if ('serviceWorker' in navigator) {
    console.log("Will the service worker register?");
    navigator.serviceWorker.register('service-worker.js')
      .then(function(reg){
        console.log("Yes, it did.");
     }).catch(function(err) {
        console.log("No it didn't. This happened:", err)
    });
 }
</script>
</html>
