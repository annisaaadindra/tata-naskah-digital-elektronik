<!DOCTYPE html>

<html>

<head>  
<meta name="theme-color" content="#00923F">
<link rel="manifest" href="manifest.json">

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>::: DINAS KETENAGAKERJAAN PEMERINTAH KOTA MEDAN :::</title>


  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->

  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">

  <!-- Font Awesome -->

  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">

  <!-- Ionicons -->

  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">

  <!-- daterange picker -->

  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">

  <!-- bootstrap datepicker -->

  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

  <!-- iCheck for checkboxes and radio inputs -->

  <link rel="stylesheet" href="plugins/iCheck/all.css">

  <!-- Bootstrap Color Picker -->

  <link rel="stylesheet" href="bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">

  <!-- Bootstrap time Picker -->

  <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">

  <!-- Select2 -->

  <link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">

  <!-- Theme style -->

  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins

   folder instead of downloading all of them to reduce the load. -->

   <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">



   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<![endif]-->



<!-- Google Font -->

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<link rel="stylesheet" href="dist/css/custom.css">
</head>

<body class="hold-transition skin-green sidebar-mini">

  <div class="wrapper">



    <?php include 'isi/capekkali/header.php';?>

    <!-- Left side column. contains the logo and sidebar -->

    <?= $_SESSION['menu'];?>



    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">

      <!-- Content Header (Page header) -->

      <section class="content-header">

        <h1>

          RESI DAN FILE SURAT MASUK

        </h1>

        <ol class="breadcrumb">

          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>

          <li class="active">Surat Masuk</li>
          <li class="active">Resi dan File Surat Masuk</li>

        </ol>

      </section>



      <!-- Main content -->

      <section class="content">

        <!-- Small boxes (Stat box) -->

        <div class="row">



          <div class="col-md-12"> 

            <div class="box box-warning"> 

              <!-- /.box-header -->

              <div class="box-body">   

                <!-- text input --> 
                <fieldset>


                  <?php
                  
                  $res_sm = mysql_query("SELECT 

                    a.*, b.unit_kerja, CONCAT('(', c.kode_masalah, ') ', c.masalah) AS masalah,

                    CONCAT('(', d.kode, ') ', d.jenis_surat) AS jenis_surat

                    FROM 

                    myapp_maintable_suratmasuk a

                    LEFT JOIN myapp_reftable_unitkerja b ON a.id_skpd_pengirim = b.id_unit_kerja

                    LEFT JOIN myapp_reftable_masalah c ON a.id_masalah = c.id_masalah

                    LEFT JOIN myapp_reftable_jenissurat d ON a.id_jenis_surat = d.id_jenis_surat

                    WHERE 

                    a.id_surat_masuk='" . $_GET["id"] . "'");

                  $ds_sm = mysql_fetch_array($res_sm);

                  ?>

                  <table border="0px" class="table table-striped" cellspacing='0' cellpadding='0' width='100%'>

                    <tr>

                      <td width='20%'>Nomor Register</td>

                      <td width='10px'>:</td>

                      <td><b><?php echo(no_register($ds_sm["noreg"])); ?></b></td>

                    </tr>

                    <tr>

                      <td width='20%'>Nomor Surat</td>

                      <td width='10px'>:</td>

                      <td><b><?php echo($ds_sm["no_surat"]); ?></b></td>

                    </tr>

                    <tr>

                      <td width='20%'>Tanggal Surat</td>

                      <td width='10px'>:</td>

                      <td><b><?php echo tglindonesia($ds_sm["tgl_surat"]); ?></b></td>

                    </tr>

                    <tr>

                      <td width='20%'>Tanggal Terima</td>

                      <td width='10px'>:</td>

                      <td><b><?php echo tglindonesia($ds_sm["tgl_terima"]); ?></b></td>

                    </tr>

                    <tr>

                      <td width='20%'>Tanggal Selesai</td>

                      <td width='10px'>:</td>

                      <td><b><?php echo tglindonesia($ds_sm["harus_selesai"]); ?></b></td>

                    </tr>

                    <tr>

                      <td width='20%'>Perihal</td>

                      <td width='10px'>:</td>

                      <td><b><?php echo($ds_sm["perihal_surat"]); ?></td>

                      </tr>

                      <tr>

                        <td width='20%'>Deskripsi</td>

                        <td width='10px'>:</td>

                        <td><b><?php echo($ds_sm["deskripsi_surat"]); ?></b></td>

                      </tr>

                      <tr>

                        <td width='20%'>SKPD / Unit Kerja Pengirim</td>

                        <td width='10px'>:</td>

                        <td><b><?php echo($ds_sm["unit_kerja"]); ?></b></td>

                      </tr>

                      <tr>

                        <td width='20%'>Masalah</td>

                        <td width='10px'>:</td>

                        <td><b><?php echo($ds_sm["masalah"]); ?></b></td>

                      </tr>

                      <tr>

                        <td width='20%'>Sub Masalah</td>

                        <td width='10px'>:</td>

                        <td><b><?php echo($ds_sm["jenis_surat"]); ?></b></td>

                      </tr>

                    </table>

                    
                    <?php 
                    if(!$_GET['edit'])
                    {

                      ?>
                      <div class="form-group col-md-3">

                        <div class="input-group">
                          <input type="button" value='Selesai dan Alihkan ke Sekretaris' class="pull-right btn btn-success" onclick="document.location.href='php/file_surat_masuk.php?id=<?=$_GET["id"]?>'" />
                        </div>

                      </div>
                      <?php
                    }
                    ?> 


                    <div class="form-group col-md-2">

                      <div class="input-group">

                        <input type="text" value='2' id="rangkap" class="form-control"><span class="input-group-btn">
                          <button class="btn btn-warning" type="button" onclick="cetakresi(<?php echo($_GET["id"]); ?>);" >Cetak (Rangkap)</button>
                        </span>
                      </div>

                    </div>

                    


                  </fieldset>

                  <form name="frm" action="php/file_sm_uploader.php" method="post" enctype="multipart/form-data">

                    <input type="hidden" name="redir" value="<?php echo($_GET["redir"]); ?>" />

                    <input type="hidden" name="id" value="<?php echo($_GET["id"]); ?>" />



                    <?php

                    if(isset($_GET["err_code"])){

                      ?>



                      <span class="err_msg"><?php echo($_GET["err_code"]) ?></span>



                      <?php

                    }

                    ?>

                    <div class="form-group col-md-6">

                      <label>Tambah File</label>

                      <input type="file" name="file" class="form-control">

                    </div>

                    <div class="form-group col-md-6">

                      <label>Keterangan File</label>

                      <input type="text" name="keterangan" class="form-control" required="">

                    </div> 
                    

                    
                    <div class="form-group col-lg-12">
                      <input class="btn btn-success" type="submit" value='Tambah File Upload'/>
                      <input class="btn btn-danger" type="reset" value='Reset File'/> 
                    </div>



                  </form>



                  <fieldset class="col-md-12">

                    <legend><h3>DAFTAR FILE BUKTI / PENDUKUNG SURAT MASUK</h3></legend>



                    <?php

                    $res = mysql_query("SELECT * FROM myapp_filetable_suratmasuk WHERE id_surat_masuk='" . $_GET["id"] . "'");

                    while($ds = mysql_fetch_array($res)){

                      ?>

                      <div class="judullist"><?php echo($ds["nama_file"]) ?></div>

                      <div class="isilist">

                        <?php echo($ds["keterangan"]) ?>

                        <div style="clear: both;"></div>

                        <a class="linktambahan btn btn-primary" href="uploaded/sm/<?php echo($ds["nama_file"]) ?>" target="_blank">Download</a>

                        <a class="linktambahan btn btn-danger" href="php/hapus_file_surat_masuk.php?id=<?=$ds["id"];?>&id_surat_masuk=<?php echo($_GET["id"]); ?>&redir=<?php echo($_GET["redir"]); ?>">Hapus</a>

                        <div style="clear: both;"></div>

                      </div>

                      <?php

                    }

                    ?>

                  </fieldset> 

                  <!-- Select multiple-->  





                </div> 

                <!-- /.box-body -->

              </div>   <br>     

            </div>

            <!-- ./col -->

          </div> 

          <!-- /.row (main row) -->



        </section>

        <!-- /.content -->

      </div>

      <!-- /.content-wrapper -->

      <?php include 'isi/capekkali/footer.php';?>



      <!-- Control Sidebar -->



      <!-- /.control-sidebar -->

  <!-- Add the sidebar's background. This div must be placed

   immediately after the control sidebar -->

   <div class="control-sidebar-bg"></div>

 </div>

 <!-- ./wrapper -->



 <!-- jQuery 3 -->

 <script src="bower_components/jquery/dist/jquery.min.js"></script>

 <!-- Bootstrap 3.3.7 -->

 <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

 <!-- Select2 -->

 <script src="bower_components/select2/dist/js/select2.full.min.js"></script>

 <!-- InputMask -->

 <script src="plugins/input-mask/jquery.inputmask.js"></script>

 <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>

 <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>

 <!-- date-range-picker -->

 <script src="bower_components/moment/min/moment.min.js"></script>

 <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

 <!-- bootstrap datepicker -->

 <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

 <!-- bootstrap color picker -->

 <script src="bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

 <!-- bootstrap time picker -->

 <script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>

 <!-- SlimScroll -->

 <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

 <!-- iCheck 1.0.1 -->

 <script src="plugins/iCheck/icheck.min.js"></script>

 <!-- FastClick -->

 <script src="bower_components/fastclick/lib/fastclick.js"></script>

 <!-- AdminLTE App -->

 <script src="dist/js/adminlte.min.js"></script>

 <!-- AdminLTE for demo purposes -->

 <script src="dist/js/demo.js"></script>

 <!-- Page script -->

 <script>

  $(function () {

    //Initialize Select2 Elements

    $('.select2').select2()



    //Datemask dd/mm/yyyy

    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })

    //Datemask2 mm/dd/yyyy

    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })

    //Money Euro

    $('[data-mask]').inputmask()



    //Date range picker

    $('#reservation').daterangepicker()

    //Date range picker with time picker

    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })

    //Date range as a button

    $('#daterange-btn').daterangepicker(

    {

      ranges   : {

        'Today'       : [moment(), moment()],

        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],

        'Last 30 Days': [moment().subtract(29, 'days'), moment()],

        'This Month'  : [moment().startOf('month'), moment().endOf('month')],

        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

      },

      startDate: moment().subtract(29, 'days'),

      endDate  : moment()

    },

    function (start, end) {

      $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

    }

    )



    //Date picker

    $('#datepicker').datepicker({

      autoclose: true

    })



    $('#datepicker1').datepicker({

      autoclose: true

    })

    $('#harus_selesai').datepicker({

      autoclose: true

    })



    //iCheck for checkbox and radio inputs

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({

      checkboxClass: 'icheckbox_minimal-blue',

      radioClass   : 'iradio_minimal-blue'

    })

    //Red color scheme for iCheck

    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({

      checkboxClass: 'icheckbox_minimal-red',

      radioClass   : 'iradio_minimal-red'

    })

    //Flat red color scheme for iCheck

    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({

      checkboxClass: 'icheckbox_flat-green',

      radioClass   : 'iradio_flat-green'

    })



    //Colorpicker

    $('.my-colorpicker1').colorpicker()

    //color picker with addon

    $('.my-colorpicker2').colorpicker()



    //Timepicker

    $('.timepicker').timepicker({

      showInputs: false

    })

  })

</script>

<script type="text/javascript">

  $(document).ready(function(){

    $("#tgl_surat").datepicker({

      dateFormat: "yy-mm-dd",

      changeMonth: true,

      changeYear: true

    });

    $("#tgl_terima").datepicker({

      dateFormat: "yy-mm-dd",

      changeMonth: true,

      changeYear: true

    });

    $("#harus_selesai").datepicker({

      dateFormat: "yy-mm-dd",

      changeMonth: true,

      changeYear: true

    });

  });

  function hapusFile(id){

            //alert("hapus");

            document.location.href = "php/hapus_file_surat_masuk.php?id=" + id + "&id_surat_masuk=<?php echo($_GET["id"]); ?>&redir=<?php echo($_GET["redir"]); ?>";

            

          }

          function goBack(){

           window.history.back();

         }

         function cetakresi(id){

          var rangkap = $("#rangkap").val();

    /*if(parseInt(rangkap)){

        //jAlert("Rangkap harus angka", "PERHATIAN");

        alert(rangkap);

    }else{

        //window.open("php/cetak_resi.php?id=" + id + "&rangkap=" + rangkap);

        alert("bukan angka");

      }*/

    //alert(rangkap);

    window.open("php/cetak_resi.php?id=" + id + "&rangkap=" + rangkap);

  }

</script>

</body>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
<script>
 if ('serviceWorker' in navigator) {
    console.log("Will the service worker register?");
    navigator.serviceWorker.register('service-worker.js')
      .then(function(reg){
        console.log("Yes, it did.");
     }).catch(function(err) {
        console.log("No it didn't. This happened:", err)
    });
 }
</script>
</html>


