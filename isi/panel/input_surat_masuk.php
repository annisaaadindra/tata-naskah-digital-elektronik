

<!DOCTYPE html>

<html>

<head>  
<meta name="theme-color" content="#00923F">
<link rel="manifest" href="manifest.json">

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>::: DINAS KETENAGAKERJAAN PEMERINTAH KOTA MEDAN :::</title>

	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<!-- Bootstrap 3.3.7 -->

	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">

	<!-- Font Awesome -->

	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">

	<!-- Ionicons -->

	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">

	<!-- Theme style -->

	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins

  	folder instead of downloading all of them to reduce the load. -->

  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  	<!-- Morris chart -->

  	<link rel="stylesheet" href="bower_components/morris.js/morris.css">

  	<!-- jvectormap -->

  	<link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">

  	<!-- Date Picker -->

  	<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

  	<!-- Daterange picker -->

  	<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">

  	<!-- bootstrap wysihtml5 - text editor -->

  	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">



  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<![endif]-->



<!-- Google Font -->

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<link rel="stylesheet" href="dist/css/custom.css">

<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">


  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<![endif]-->

<style>

label.error {

	color: red; font-size:12px; font-family:verdana;

}

#sisa{ 

}

</style> 

<!-- Google Font -->

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<link rel="stylesheet" href="dist/css/custom.css">
</head>

<body class="hold-transition skin-green sidebar-mini">

	<div class="wrapper">



		<?php include 'isi/capekkali/header.php';?>

		<!-- Left side column. contains the logo and sidebar -->

		<?= $_SESSION['menu'];?>



		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->

			<section class="content-header">

				<h1>

					INPUT SURAT MASUK

				</h1>

				<ol class="breadcrumb">

					<li><a href="./?mod=main_loket"><i class="fa fa-dashboard"></i> Beranda </a></li>
					<li>Surat Masuk</li>

					<li class="active">Input Surat Masuk</li>

				</ol>

			</section>



			<!-- Main content -->

			<section class="content">

				<!-- Small boxes (Stat box) -->

				<div class="row">



					<div class="col-md-12" style='padding-top:10px;'> 

						<div class="box box-warning"> 

							<!-- /.box-header -->

							<div class="box-body">  

								<form name="frm" action="php/input_surat_masuk.php" method="POST" id="frm_input_sm" enctype="multipart/form-data">  

									<!-- text input -->



									<div class="form-group col-md-6">

										<label>Nomor Surat</label>

										<input type="text" name="no_surat" class="form-control" required>

									</div>

									<div class="form-group col-md-6">



										<label>Tanggal Surat:</label>



										<div class="input-group date">

											<div class="input-group-addon">

												<i class="fa fa-calendar"></i>

											</div>

											<input type="text" name="tgl_surat" class="form-control pull-right" id="datepicker" required>

										</div>

										<!-- /.input group -->

									</div>

									<div class="form-group col-md-6">

										<label>Tanggal Terima:</label>



										<div class="input-group date">

											<div class="input-group-addon">

												<i class="fa fa-calendar"></i>

											</div>

											<input type="text" name="tgl_terima" class="form-control pull-right" id="datepicker1" required>

										</div>

										<!-- /.input group -->

									</div>

									<div class="form-group col-md-6">

										<label>Perihal</label>

										<input type="text" name="perihal_surat" class="form-control" required>

									</div>  

									<!-- textarea -->

									<div class="form-group col-md-6">

										<label>Alamat Pengirim</label>

										<textarea class="form-control" name="alamat_pengirim" rows="3" required></textarea>

									</div> 

									<div class="form-group col-md-6">

										<label>Judul Surat</label>

										<textarea class="form-control" name="judul_surat" rows="3" required></textarea>

									</div> 

									<div class="form-group col-md-6">

										<label>Deskripsi Surat</label>

										<textarea class="form-control" name="deskripsi_surat" rows="3" required></textarea>

									</div> 

									<div class="form-group col-md-6">

										<label>Catatan Tambahan</label>

										<textarea class="form-control" name="catatan" rows="3"></textarea>

									</div> 



									<!-- select -->



									<div class="form-group col-md-6">

										<label>Pilih Jenis Instansi Pengirim</label>

										<select class="form-control" name="" onchange="loadtingkatan(this.value);">

											<option value="0" hidden>[.. Pilih Jenis Instansi Pengirim ..]</option>
											<option value='1'>Instansi Pemerintahan</option>
											<option value='2'>Instansi NON Pemerintahan</option>
											

										</select>

									</div>

									<div class="form-group col-md-6">

										<label>Pilih Bentuk / Tingkatan Instansi Pengirim</label>

										<select class="form-control" name="" id="tingkatan" onchange="loadinstansi(this.value);">

											<option value="0" hidden>[.. Pilih Bentuk / Tingkatan Instansi Pengirim ..]</option> 

										</select>

									</div>


									<div class="form-group col-md-6">

										<label>Pilih Instansi Pengirim</label>

										<select class="form-control" name="id_skpd_pengirim" id="id_skpd_pengirim">

											<option value="0" hidden>[.. Pilih Instansi Pengirim ..]</option>
										</select>

									</div>



									<div class="form-group col-md-6">

										<label>Masalah</label>

										<select class="form-control"  name="id_masalah" onchange="loadsubmasalah(this.value);">

											<option value="0" hidden>[.. Pilih Masalah ..]</option>

											<?php

											$res_masalah= mysql_query("SELECT * FROM myapp_reftable_masalah ORDER BY kode_masalah ASC");

											while($ds_masalah = mysql_fetch_array($res_masalah)){

												echo("<option value='" . $ds_masalah["id_masalah"] . "'>(" . $ds_masalah["kode_masalah"] . ") " . $ds_masalah["masalah"] . "</option>");

											}

											?>

										</select>

									</div> 

									<div class="form-group col-md-6">
										<label>Sub Masalah</label>

										<select class="form-control" name="id_jenis_surat" id="id_jenis_surat">

											<option value="0">[.. Pilih Sub Masalah ..]</option>

										</select>

									</div>

									



									<div class="form-group col-md-6">

										<label>Harus Selesai Pada</label>



										<div class="input-group date">

											<div class="input-group-addon">

												<i class="fa fa-calendar"></i>

											</div>

											<input type="text" name="harus_selesai" id="harus_selesai" class="form-control pull-right" placeholder="*) Kosongkan jika tidak perlu ditindak lanjuti">

										</div>

										<!-- /.input group -->

									</div> 



									<!-- Select multiple-->  





									<div class="form-group col-md-6">  

										<input type="hidden" required name="redir" value="<?php echo($_GET["redir"]); ?>" />

										<input type="hidden" name="id" value="<?php echo($_GET["id"]); ?>" />

										<label> Upload File</label>
										
										<input type="file" name="file" class="form-control"/>
									</div>

									<div class="form-group col-md-6">
										<label>Keterangan File</label>
										<input type="text" name="keterangan" class="form-control" />
									</div>


								</div>


								<div class="box-footer" style="margin-left: 15px">



									<input type="submit" value='Simpan' class="btn btn-success" onclick='document.location.href=\"info.php?pesan=1"\'> 

									<input type="reset" value='Reset' class="btn btn-danger">   

								</div>

							</div>

						</div>



					</div>

				</form>

				<!-- /.box-body -->

			</div> 

		</div>

		<!-- ./col -->

	</div> 

	<!-- /.row (main row) -->



</section>

<!-- /.content -->

</div>

<!-- /.content-wrapper -->

<?php include 'isi/capekkali/footer.php';?>



	<!-- Control Sidebar -->



	<!-- /.control-sidebar -->

  <!-- Add the sidebar's background. This div must be placed

  	immediately after the control sidebar -->

  	<div class="control-sidebar-bg"></div>

  </div>

  <!-- ./wrapper -->



  <!-- jQuery 3 -->

  <script src="bower_components/jquery/dist/jquery.min.js"></script>



  <script src="cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>

  <!-- Bootstrap 3.3.7 -->

  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

  <!-- Select2 -->

  <script src="bower_components/select2/dist/js/select2.full.min.js"></script>

  <!-- InputMask -->

  <script src="plugins/input-mask/jquery.inputmask.js"></script>

  <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>

  <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>

  <!-- date-range-picker -->

  <script src="bower_components/moment/min/moment.min.js"></script>

  <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

  <!-- bootstrap datepicker -->

  <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

  <!-- bootstrap color picker -->

  <script src="bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

  <!-- bootstrap time picker -->

  <script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>

  <!-- SlimScroll -->

  <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

  <!-- iCheck 1.0.1 -->

  <script src="plugins/iCheck/icheck.min.js"></script>

  <!-- FastClick -->

  <script src="bower_components/fastclick/lib/fastclick.js"></script>

  <!-- AdminLTE App -->

  <script src="dist/js/adminlte.min.js"></script>

  <!-- AdminLTE for demo purposes -->

  <script src="dist/js/demo.js"></script>

  <!-- Page script -->

  <script>

  	$(function () {

    //Initialize Select2 Elements

    $('.select2').select2()



    //Datemask dd/mm/yyyy

    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })

    //Datemask2 mm/dd/yyyy

    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })

    //Money Euro

    $('[data-mask]').inputmask()



    //Date range picker

    $('#reservation').daterangepicker()

    //Date range picker with time picker

    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })

    //Date range as a button

    $('#daterange-btn').daterangepicker(

    {

    	ranges   : {

    		'Today'       : [moment(), moment()],

    		'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

    		'Last 7 Days' : [moment().subtract(6, 'days'), moment()],

    		'Last 30 Days': [moment().subtract(29, 'days'), moment()],

    		'This Month'  : [moment().startOf('month'), moment().endOf('month')],

    		'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

    	},

    	startDate: moment().subtract(29, 'days'),

    	endDate  : moment()

    },

    function (start, end) {

    	$('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

    }

    )



    //Date picker

    $('#datepicker').datepicker({



    	autoclose: true



    })



    $('#datepicker1').datepicker({

    	autoclose: true

    })

    $('#harus_selesai').datepicker({

    	autoclose: true

    })



    //iCheck for checkbox and radio inputs

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({

    	checkboxClass: 'icheckbox_minimal-blue',

    	radioClass   : 'iradio_minimal-blue'

    })

    //Red color scheme for iCheck

    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({

    	checkboxClass: 'icheckbox_minimal-red',

    	radioClass   : 'iradio_minimal-red'

    })

    //Flat red color scheme for iCheck

    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({

    	checkboxClass: 'icheckbox_flat-green',

    	radioClass   : 'iradio_flat-green'

    })



    //Colorpicker

    $('.my-colorpicker1').colorpicker()

    //color picker with addon

    $('.my-colorpicker2').colorpicker()



    //Timepicker

    $('.timepicker').timepicker({

    	showInputs: false

    })

})

  	$(document).on("focus keyup", "input.autocomplete", function() {

    // Cache useful selectors

    var $input = $(this);

    var $dropdown = $input.next("ul.dropdown-menu");

    

    // Create the no matches entry if it does not exists yet

    if (!$dropdown.data("containsNoMatchesEntry")) {

    	$("input.autocomplete + ul.dropdown-menu").append('<li class="no-matches hidden"><i>(Tambah Baru)</i></li>');

    	$dropdown.data("containsNoMatchesEntry", true);

    } 

    

    // Show only matching values

    $dropdown.find("li:not(.no-matches)").each(function(key, li) {

    	var $li = $(li);

    	$li[new RegExp($input.val(), "i").exec($li.text()) ? "removeClass" : "addClass"]("hidden");

    });

    

    // Show a specific entry if we have no matches

    $dropdown.find("li.no-matches")[$dropdown.find("li:not(.no-matches):not(.hidden)").length > 0 ? "addClass" : "removeClass"]("hidden");

});

  	$(document).on("click", "input.autocomplete + ul.dropdown-menu li", function(e) {

    // Prevent any action on the window location

    e.preventDefault();

    

    // Cache useful selectors

    $li = $(this);

    $input = $li.parent("ul").prev("input");

    

    // Update input text with selected entry

    if (!$li.is(".no-matches")) {

    	$input.val($li.text());

    }

});

</script>

<script type="text/javascript" src="./libraries/jquery.validate.js"></script>

<script type="text/javascript">



	$(document).ready(function(){ 

		$("#sisa").hide();

  $("#frm_input_sm").validate(); // call the jquery validate to validate input user

  

  $("#tgl_surat").datepicker({

  	dateFormat: "yy-mm-dd",

  	changeMonth: true,

  	changeYear: true

  });

  

  $("#tgl_terima").datepicker({

  	dateFormat: "yy-mm-dd",

  	changeMonth: true,

  	changeYear: true

  });

  $("#harus_selesai").datepicker({

  	dateFormat: "yy-mm-dd",

  	changeMonth: true,

  	changeYear: true

  });

  

});



	function loadsubmasalah(id_masalah){

    //alert(id_masalah);

    $("#id_jenis_surat").html("<option hidden value='0'>[.. Pilih Sub Masalah ..]</option>");

    $.ajax({

    	type: "GET",

    	url: "ajax/submasalah.php",

    	data: "id_masalah=" + id_masalah,

    	success: function(r){

            //alert(r);

            $("#id_jenis_surat").append(r);

        }

    });

}

function loadtingkatan(tingkatan){

    //alert(tingkatan);

    $("#tingkatan").html("<option value='0' hidden>[.. Pilih Tingkatan ..]</option>");

    $.ajax({

    	type: "GET",

    	url: "ajax/tingkatan.php",

    	data: "tingkatan=" + tingkatan,

    	success: function(r){

            //alert(r);

            $("#tingkatan").append(r);

        }

    });

}

function loadinstansi(id_skpd_pengirim){

    //alert(id_skpd_pengirim);

    $("#id_skpd_pengirim").html("<option hidden value='0'>[.. Pilih Tingkatan ..]</option>");

    $.ajax({

    	type: "GET",

    	url: "ajax/instansi.php",

    	data: "id_skpd_pengirim=" + id_skpd_pengirim,

    	success: function(r){

             //alert(r);

             $("#id_skpd_pengirim").append(r);

         }

     });

}


 



</script>



</body>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
<script>
 if ('serviceWorker' in navigator) {
    console.log("Will the service worker register?");
    navigator.serviceWorker.register('service-worker.js')
      .then(function(reg){
        console.log("Yes, it did.");
     }).catch(function(err) {
        console.log("No it didn't. This happened:", err)
    });
 }
</script>
</html>


