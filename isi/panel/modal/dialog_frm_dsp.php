<head>  
<meta name="theme-color" content="#00923F">
<link rel="manifest" href="manifest.json">


  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->

  <link rel="stylesheet" href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css">

  <!-- Font Awesome -->

  <link rel="stylesheet" href="../../../bower_components/font-awesome/css/font-awesome.min.css">

  <!-- Ionicons -->

  <!--link rel="stylesheet" href="../../../bower_components/Ionicons/css/ionicons.min.css">

    <!-- dtt -->

    <link rel="stylesheet" href="../../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Theme style -->

    <link rel="stylesheet" href="../../../dist/css/AdminLTE.min.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins

    folder instead of downloading all of them to reduce the load. -->

    <link rel="stylesheet" href="../../../dist/css/skins/_all-skins.min.css">

    <!-- Morris chart -->

    <link rel="stylesheet" href="../../../bower_components/morris.js/morris.css">

    <!-- jvectormap -->

    <link rel="stylesheet" href="../../../bower_components/jvectormap/jquery-jvectormap.css">

    <!-- Date Picker -->

    <link rel="stylesheet" href="../../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- Daterange picker -->

    <link rel="stylesheet" href="../../../bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="../../../dist/css/custom.css">


    <!-- bootstrap wysihtml5 - text editor -->

    <!--link rel="stylesheet" href="../../../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"-->


</head>

<body>

 <div class="container well">

    <?php
    session_start();
error_reporting(0);
    include "../../../php/koneksi.php";
    include "../../../method/function.php";
    
    $id_surat_masuk = anti_injection($_GET['id_surat_masuk']);
    $id_disposisi = anti_injection($_GET['id_disposisi']);
    
    ?>
    <!-- DIALOG -->
    <fieldset>
      <legend><h3>Lanjutkan Surat Ke Arsip <?=" (ID Surat: ".$id_surat_masuk.")";?></h3></legend>
      <form name="frm" action="../../../php/proses_ulang_surat_masuk.php" method="POST">
        <input type="hidden" name="id_surat_masuk" value="<?=$id_surat_masuk;?>" id="id_surat_masuk" />
        <input type="hidden" name="id_disposisi" value="<?=$id_disposisi;?>" id="id_disposisi" />
        <div class="form-group col-md-6">
          <label>
            Catatan Disposisi
        </label>
        <textarea class="form-control" name="catatan" id="catatan_1" placeholder="Pilih Kalimat Disposisi" required=""></textarea>



    </div>
    <div class="form-group col-md-6">
      <label>
        Input File
    </label>
    <input type="file" name="file" />



</div>
<div class="form-group col-md-12">

  <input type="submit" name="arsip" class="btn btn-success" value='Kirim ke Arsip'/> 

</div>  
</form>
</fieldset> 

<!-- END OF DIALOG -->