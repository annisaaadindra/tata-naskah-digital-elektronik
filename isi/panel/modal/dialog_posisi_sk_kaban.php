<head>  
<meta name="theme-color" content="#00923F">
<link rel="manifest" href="manifest.json">


    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap 3.3.7 -->

    <link rel="stylesheet" href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="../../../bower_components/font-awesome/css/font-awesome.min.css">

    <!-- Ionicons -->

  <!--link rel="stylesheet" href="../../../bower_components/Ionicons/css/ionicons.min.css">

      <!-- dtt -->

      <link rel="stylesheet" href="../../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

      <!-- Theme style -->

      <link rel="stylesheet" href="../../../dist/css/AdminLTE.min.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins

      folder instead of downloading all of them to reduce the load. -->

      <link rel="stylesheet" href="../../../dist/css/skins/_all-skins.min.css">

      <!-- Morris chart -->

      <link rel="stylesheet" href="../../../bower_components/morris.js/morris.css">

      <!-- jvectormap -->

      <link rel="stylesheet" href="../../../bower_components/jvectormap/jquery-jvectormap.css">

      <!-- Date Picker -->

      <link rel="stylesheet" href="../../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

      <!-- Daterange picker -->

      <link rel="stylesheet" href="../../../bower_components/bootstrap-daterangepicker/daterangepicker.css">
      <link rel="stylesheet" href="../../../dist/css/custom.css">


      <!-- bootstrap wysihtml5 - text editor -->

      <!--link rel="stylesheet" href="../../../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"-->


  </head>

  <body>

   <div class="container well">
    <?php
    session_start();
error_reporting(0);
    include "../../../php/koneksi.php";
    include "../../../method/function.php";

    $id_surat_keluar = anti_injection($_GET['id_surat_keluar']);
    $id_disposisi = anti_injection($_GET['id_disposisi']);
    $no_din = anti_injection($_GET['no_din']);
    $tgl_din = anti_injection($_GET['tgl_din']);

    ?>
    <script src='../../../libraries/jquery-1.4.3.js'></script>
    <link type="text/css" href="../../../libraries/development-bundle/themes/base/ui.all.css" rel="stylesheet" />
    <script type="text/javascript" src="../../../libraries/development-bundle/ui/ui.core.js"></script>
    <script type="text/javascript" src="../../../libraries/development-bundle/ui/ui.datepicker.js"></script>
    <script type="text/javascript" src="../../../libraries/development-bundle/ui/i18n/ui.datepicker-id.js"></script>
    <script type="text/javascript">
     $("#tgl_nodin").datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true
    });
</script>
<!-- DIALOG -->
<fieldset>
 <legend><h3>Disposisi Surat</h3></legend>
 <form name="frm" action="../../../php/posisi_surat_keluar_kaban.php" method="post">
    <input type="hidden" name="id_surat_keluar" value="<?=$id_surat_keluar;?>" id="id_surat_keluar" />
    <input type="hidden" name="id_disposisi" value="<?=$id_disposisi;?>" id="id_disposisi" />  
    <div class="form-group col-md-6">
        <label>
          Catatan Disposisi
      </label>
      <textarea class="form-control" name="catatan" id="catatan_1" placeholder="Pilih Kalimat Disposisi" required></textarea>



  </div>
  <div class="form-group col-md-12">
    <input type="submit" class="btn btn-success" name="terima" value='Sudah di Tanda Tangani'/>
            <input type="submit" class="btn btn-danger" name="tolak" value='Perlu Perbaikan'/>

</div> 
</form>
</fieldset>
<!-- END OF DIALOG -->

</div>
</body>