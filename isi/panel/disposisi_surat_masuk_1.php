
<!DOCTYPE html>
<html>
<head>  
<meta name="theme-color" content="#00923F">
<link rel="manifest" href="manifest.json">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>::: DINAS KETENAGAKERJAAN PEMERINTAH KOTA MEDAN :::</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link rel="stylesheet" href="dist/css/custom.css">
</head>
<body class="hold-transition skin-green sidebar-mini">
  <div class="wrapper">

    <?php include 'isi/capekkali/header.php';?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>Loker</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- search form --> 
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">NAVIGASI UTAMA</li> 
          <li><a href="./?mod=input_surat_masuk"><i class="fa fa-book"></i> <span>Input Data Surat Masuk</span></a></li> 
          <li><a href="./?mod=manajemen_surat_masuk_1"><i class="fa fa-book"></i> <span>Manajemen Surat Masuk</span></a></li> 
          <li><a href="./?mod=proses_ulang_surat_masuk"><i class="fa fa-book"></i> <span>Proses Ulang Surat Masuk</span></a></li> 
          <li><a href="./?mod=disposisi_surat_masuk_1"><i class="fa fa-book"></i> <span>Detail Posisi Surat Masuk</span></a></li> 
          <li><a href="./?mod=finalisasi_surat_keluar"><i class="fa fa-book"></i> <span>Upload File Final Surat Keluar</span></a></li> 
          <li><a href="./?mod=lembar_kendali"><i class="fa fa-book"></i> <span>Lembar Kendali</span></a></li> 
          <li><a href="./?mod=laporan_loket"><i class="fa fa-book"></i> <span>Laporan-laporan</span></a></li> 
          <li><a href="./?mod=pengguna"><i class="fa fa-book"></i> <span>Profil User</span></a></li> 
          <li><a href="./php/nonlogin/logout.php"><i class="fa fa-book"></i> <span>Logout</span></a></li>  
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Detail Posisi
          <small>Surat Masuk</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda Loker</a></li>
          <li class="active">Input Surat Masuk</li>
        </ol>
      </section>
      
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">

          <div class="col-md-12" style='padding-top:10px;'> 
            <div class="box box-warning"> 
              <!-- /.box-header -->
              <div class="box-body">   
                <fieldset>
                  <legend><h3>DAFTAR SURAT MASUK YANG AKAN DIDISPOSISIKAN KE BIDANG</h3></legend>
                  
                  <?php
                  $res = mysql_query("SELECT 
                   a.*, b.unit_kerja
                   FROM 
                   myapp_maintable_suratmasuk a
                   LEFT JOIN myapp_reftable_unitkerja b ON a.id_skpd_pengirim = b.id_unit_kerja
                   WHERE
                   status = 1
                   ORDER BY 
                   id ASC");
                  $ctr = 0;
                  while($ds = mysql_fetch_array($res)){
                    $ctr++;
                    
                    ?>
                    <table border="0px" cellspacing='3' cellpadding='2' width='100%'>
                     
                      <tr>
                        <td rowspan="7" width='25px' style="background-color: green; color: white;"><center><?php echo($ctr); ?></center></td>
                        <td>Nomor Surat</td>
                        <td width='10px'>:</td>
                        <td><span class="detail"><?php echo($ds["no_surat"]); ?></span></td>
                      </tr>
                      <tr>
                        <td>Tanggal Surat</td>
                        <td width='10px'>:</td>
                        <td><span class="detail"><?php echo($ds["tgl_surat"]); ?></span></td>
                      </tr>
                      <tr>
                        <td>Tanggal Terima</td>
                        <td width='10px'>:</td>
                        <td><span class="detail"><?php echo($ds["tgl_terima"]); ?></span></td>
                      </tr>
                      <tr>
                        <td>Perihal</td>
                        <td width='10px'>:</td>
                        <td><span class="detail"><?php echo($ds["perihal_surat"]); ?></span></td>
                      </tr>
                      <tr>
                       <td></td>
                       <td></td>
                       <td>
                        <select id="tujuan_disposisi_<?php echo($ds["id"]); ?>" style='width:200px;'>
                          <option value="0">[.. Pilih Tujuan Disposisi ..]</option>
                          <?php
                          $res_ldb = mysql_query("SELECT * FROM myapp_reftable_levelpengguna WHERE atasan='" . $_SESSION["id_level"] . "'");
                          while($ds_ldb = mysql_fetch_array($res_ldb)){
                            echo("<option value='" . $ds_ldb["id"] . "'>" . $ds_ldb["level"] . "</option>");
                          }
                          ?>
                        </select>
                      </td>
                    </tr><br/>
                    <tr>
                     <td></td>
                     <td></td>
                     <td>
                      <textarea id="catatan_<?php echo($ds["id"]); ?>" placeholder=" :: Berikan Catatan Pada Disposisi ::" rows='9' cols='30'></textarea>
                    </td>
                  </tr><br/><br/>
                  <tr>
                   <td></td>
                   <td></td>
                   <td>
                    <input type="button" value="Kirim Disposisi" onclick="disposisi(<?php echo($ds["id"]) ?>);" />
                  </td>
                </tr>
              </table>
              <?php
              
            }
            ?>
          </table>
        </fieldset> 
      </div> 
      <!-- /.box-body -->
    </div>   <br>     
  </div>
  <!-- ./col -->
</div> 
<!-- /.row (main row) -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include 'isi/capekkali/footer.php';?>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
   <div class="control-sidebar-bg"></div>
 </div>
 <!-- ./wrapper -->

 <!-- jQuery 3 -->
 <script src="bower_components/jquery/dist/jquery.min.js"></script>

 <script src="cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>
 <!-- Bootstrap 3.3.7 -->
 <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
 <!-- Select2 -->
 <script src="bower_components/select2/dist/js/select2.full.min.js"></script>
 <!-- InputMask -->
 <script src="plugins/input-mask/jquery.inputmask.js"></script>
 <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
 <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
 <!-- date-range-picker -->
 <script src="bower_components/moment/min/moment.min.js"></script>
 <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
 <!-- bootstrap datepicker -->
 <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
 <!-- bootstrap color picker -->
 <script src="bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
 <!-- bootstrap time picker -->
 <script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
 <!-- SlimScroll -->
 <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
 <!-- iCheck 1.0.1 -->
 <script src="plugins/iCheck/icheck.min.js"></script>
 <!-- FastClick -->
 <script src="bower_components/fastclick/lib/fastclick.js"></script>
 <!-- AdminLTE App -->
 <script src="dist/js/adminlte.min.js"></script>
 <!-- AdminLTE for demo purposes -->
 <script src="dist/js/demo.js"></script>
 <!-- Page script -->
 <script type="text/javascript">
  function disposisi(id){
   var konfirm = confirm("Anda yakin akan mendisposisikan data surat masuk ini?");
   if(konfirm){
     var tujuan_disposisi = $("#tujuan_disposisi_" + id).val();
     var catatan = $("#catatan_" + id).val();
            //alert("KE : " + tujuan_disposisi + "\nCatatan : " + catatan);
            document.location.href = "php/disposisi_surat_masuk_1.php?id=" + id + "&tujuan_disposisi=" + tujuan_disposisi + "&catatan=" + catatan;
          }else{
		// do nothing
	}
}
</script>
</body>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
<script>
 if ('serviceWorker' in navigator) {
    console.log("Will the service worker register?");
    navigator.serviceWorker.register('service-worker.js')
      .then(function(reg){
        console.log("Yes, it did.");
     }).catch(function(err) {
        console.log("No it didn't. This happened:", err)
    });
 }
</script>
</html>


