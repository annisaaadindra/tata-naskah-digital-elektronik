 



<!DOCTYPE html>
<html>
<head>  
<meta name="theme-color" content="#00923F">
<link rel="manifest" href="manifest.json">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>::: DINAS KETENAGAKERJAAN PEMERINTAH KOTA MEDAN :::</title>

	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  	<!-- Morris chart -->
  	<link rel="stylesheet" href="bower_components/morris.js/morris.css">
  	<!-- jvectormap -->
  	<link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  	<!-- Date Picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  	<!-- Daterange picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  	<!-- bootstrap wysihtml5 - text editor -->
  	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link rel="stylesheet" href="dist/css/custom.css">
</head>
<body class="hold-transition skin-green sidebar-mini">
	<div class="wrapper">

		<?php include 'isi/capekkali/header.php';?>
		<!-- Left side column. contains the logo and sidebar -->
		<?= $_SESSION['menu'] ?>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Dashboard
					<small>Control panel</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active">Dashboard</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				<!-- Small boxes (Stat box) --> 
				<div class="row">
					<!-- Left col -->
					<section class="col-lg-12 connectedSortable">
						<!-- Custom tabs (Charts with tabs)--> 
						<!-- /.nav-tabs-custom -->

						<!-- Chat box -->
						<div class="box box-warning">
							<div class="box-header"> 
								
							</div>
							<div class="box-body">
								
								<h3>INPUT FILE BUKTI / PENDUKUNG SURAT KELUAR</h3><br/>
								
								<form name="frm" action="php/file_surat_keluar.php" method="post" enctype="multipart/form-data">
									<input type="hidden" name="saya" value="1" />
									<input type="hidden" name="id" value="<?php echo($_GET["id"]); ?>" />
									<table border="0px" cellspacing='0' cellpadding='0' width='100%'>
										<?php
										if(isset($_GET["err_code"])){
											?>
											<tr>
												<td colspan="3"><span class="err_msg"><?php echo($_GET["err_code"]) ?></span></td>
											</tr>
											<?php
										}
										?>
										<tr>
											<td width='20%'>ID Surat</td>
											<td width='10px'>:</td>
											<td><input type="text" name="id_surat" value="<?php echo($_GET["id"]); ?>" readonly="readonly"/></td>
										</tr>
										<tr>
											<td width='20%'>Upload File</td>
											<td width='10px'>:</td>
											<td><input type="file" name="file" /></td>
										</tr>
										<tr>
											<td width='20%'>Keterangan File</td>
											<td width='10px'>:</td>
											<td><input type="text" name="keterangan" /></td>
										</tr>                
										<tr>
											<td width='20%'>Tipe File</td>
											<td width='10px'>:</td>
											<td>
												<input type="hidden" name="status" value="4" />
												<b>File Final Surat Keluar (Berkas-Berkas yang sudah ditanda tangani)</b>
											</td>
										</tr>
									</table>
									<br />
									
									<table border="0px" cellspacing='0' cellpadding='0' width='100%'>
										<tr>
											<td>&nbsp;</td>
											<?php
											$redir = "?mod=";
											if(isset($_GET["redir"])){
												$redir = "?mod=" . $_GET["redir"] . "&id=" . $_GET["id"];
											}
											?>
											<td width='35%'><input type="submit" value='Simpan' style="width: 80%;" />&nbsp;</td>
											<td width='35%'><input type="reset" value='Reset' style="width: 80%;" />&nbsp;</td>
										</tr>
									</table>
								</form><br/>
								<fieldset>
									<legend><h3>DAFTAR FILE BUKTI / PENDUKUNG SURAT KELUAR</h3></legend>

									<?php
									$res = mysql_query("SELECT * FROM myapp_filetable_suratkeluar WHERE id_surat_keluar='" . $_GET["id"] . "' AND status = 4");
									while($ds = mysql_fetch_array($res)){
										?>
										<div class="judullist"><?php echo($ds["nama_file"]) ?></div>
										<div class="isilist">
											<?php echo($ds["keterangan"]) ?>
											<div style="clear: both;"></div>
											<a class="linktambahan" href="uploaded/sk/<?php echo($ds["nama_file"]) ?>" target="_blank">Download</a>
											<a class="linktambahan" href="javascript:hapusFile(<?php echo($ds["id"]); ?>);">Hapus</a>
											<div style="clear: both;"></div>
										</div>
										<?php
									}
									?>

								</fieldset>
							</div>
							<!-- /.chat --> 
						</div>
						<!-- /.box (chat box) -->

						<!-- TO DO List --> 
						<!-- /.Left col -->
						<!-- right col (We are only adding the ID to make the widgets sortable)--> 
					</div>
					<!-- /.row (main row) -->

				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
			<?php include 'isi/capekkali/footer.php';?>

				<!-- Control Sidebar -->
				
				<!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  	immediately after the control sidebar -->
  	<div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
  	$.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.7 -->
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Morris.js charts -->
  <script src="bower_components/raphael/raphael.min.js"></script>
  <script src="bower_components/morris.js/morris.min.js"></script>
  <!-- Sparkline -->
  <script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
  <!-- jvectormap -->
  <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="bower_components/moment/min/moment.min.js"></script>
  <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <!-- Slimscroll -->
  <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.min.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
  <script type="text/javascript">
  	$(document).ready(function(){
  		$("#tgl_surat").datepicker({
  			dateFormat: "yy-mm-dd",
  			changeMonth: true,
  			changeYear: true
  		});
  		$("#tgl_terima").datepicker({
  			dateFormat: "yy-mm-dd",
  			changeMonth: true,
  			changeYear: true
  		});
  		$("#harus_selesai").datepicker({
  			dateFormat: "yy-mm-dd",
  			changeMonth: true,
  			changeYear: true
  		});
  	});
  	function hapusFile(id){
  		var konfirm = confirm("Anda yakin akan menghapus data file ini?");
  		if(konfirm){
  			document.location.href = "php/hapus_file_surat_keluar.php?id=" + id + "&id_surat_masuk=<?php echo($_GET["id"]); ?>&redir=<?php echo($_GET["redir"]); ?>";
		 //alert("hapus");
		}else{
		// do nothing
	}
	
}
</script>
</body>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
<script>
 if ('serviceWorker' in navigator) {
    console.log("Will the service worker register?");
    navigator.serviceWorker.register('service-worker.js')
      .then(function(reg){
        console.log("Yes, it did.");
     }).catch(function(err) {
        console.log("No it didn't. This happened:", err)
    });
 }
</script>
</html>
   



