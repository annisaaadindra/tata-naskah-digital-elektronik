

<!DOCTYPE html>

<html>

<head>  
	<meta name="theme-color" content="#00923F">
<link rel="manifest" href="manifest.json">

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>::: DINAS KETENAGAKERJAAN PEMERINTAH KOTA MEDAN :::</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<!-- Bootstrap 3.3.7 -->

	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">

	<!-- Font Awesome -->

	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">

	<!-- Ionicons -->

	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">

	<!-- Theme style -->

	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins

  	folder instead of downloading all of them to reduce the load. -->

  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  	<!-- Morris chart -->

  	<link rel="stylesheet" href="bower_components/morris.js/morris.css">

  	<!-- jvectormap -->

  	<link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">

  	<!-- Date Picker -->

  	<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

  	<!-- Daterange picker -->

  	<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">

  	<!-- bootstrap wysihtml5 - text editor -->

  	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">



  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<![endif]-->



<!-- Google Font -->

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<link rel="stylesheet" href="dist/css/custom.css">

<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">

</head>

<body class="hold-transition skin-green sidebar-mini">

	<div class="wrapper">



		<?php include 'isi/capekkali/header.php';?>

		<!-- Left side column. contains the logo and sidebar -->

		<?= $_SESSION['menu'];?>



		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->

			<section class="content-header">

				<h1>

					UPLOAD FILE FINAL SURAT KELUAR

				</h1>

				<ol class="breadcrumb">

					<li><a href="./"><i class="fa fa-dashboard"></i> Beranda</a></li>
					<li>Surat Keluar</li>

					<li class="active">Upload File Final Surat Keluar</li>

				</ol>

			</section>



			<!-- Main content -->

			<section class="content">

				<!-- Small boxes (Stat box) -->

				<div class="row">



					<div class="col-md-12" style='padding-top:10px;'> 

						<div class="box box-warning"> 

							<!-- /.box-header -->

							<div class="box-body">      



								<fieldset> 
									<table id="example2" class="listingtable table table-bordered table-striped">

										<tfoot>

											<tr>

												<th>NO.</th>

												<th>NO. REGISTER</th>

												<th>NO. SURAT</th>

												<th>TGL. SURAT</th>

												<th>TGL. TERIMA</th>

												<th>PERIHAL</th>

												<th>UNIT PENGIRIM</th>

												<th>ASAL DISPOSISI</th>

												<th>TIPE DISPOSISI</th>

												<th style="display: none">&nbsp;</th>

												<th style="display: none">&nbsp;</th>

												<th style="display: none">&nbsp;</th>

												<th style="display: none">&nbsp;</th>

												<th style="display: none">&nbsp;</th>

											</tr>

										</tfoot>

										<thead>

											<tr>

												<th>NO.</th>

												<th>NO. REGISTER</th>

												<th>NO. SURAT</th>

												<th>TGL. SURAT</th>

												<th>TGL. TERIMA</th>

												<th>PERIHAL</th>

												<th>UNIT PENGIRIM</th>

												<th>ASAL DISPOSISI</th>

												<th>TIPE DISPOSISI</th>

												<th width='30px'>DETAIL SURAT YG DIBALAS</th> 
												<th width='30px'>DETAIL</th> 
												<th width='30px'>RIWAYAT</th> 
												<th width='30px'>LAMPIRAN</th> 
												<th width='30px'>DISPOSISI</th> 

											</tr>

										</thead>

										<tbody>

											<?php

											$whr = "";

											$sql = "";

											if(isset($_POST["ftype"]) && $_POST["ftype"] == 1 || empty($_POST["ftype"])){

												if(isset($_POST["id"]) && $_POST["id"] <> "")

													$whr .= " AND a.id = '" . $_POST["id"] . "'";

												if(isset($_POST["no_surat"]) && $_POST["no_surat"] <> "")

													$whr .= " AND a.no_surat LIKE '%" . $_POST["no_surat"] . "%'";

												if(isset($_POST["tgl_surat_dari"]) && isset($_POST["tgl_surat_sampai"]) && $_POST["tgl_surat_dari"] <> "" && $_POST["tgl_surat_sampai"] <> "")

													$whr .= " AND a.tgl_surat BETWEEN '" . $_POST["tgl_surat_dari"] . "' AND '" . $_POST["tgl_surat_sampai"] . "'";

												if(isset($_POST["tgl_terima_dari"]) && isset($_POST["tgl_terima_sampai"]) && $_POST["tgl_terima_dari"] <> "" && $_POST["tgl_terima_sampai"] <> "")

													$whr .= " AND a.tgl_kirim BETWEEN '" . $_POST["tgl_terima_dari"] . "' AND '" . $_POST["tgl_terima_sampai"] . "'";

												if(isset($_POST["perihal_surat"]) && $_POST["perihal_surat"] <> "")

													$whr .= " AND a.perihal_surat LIKE '%" . $_POST["perihal_surat"] . "%'";

												if(isset($_POST["deskripsi_surat"]) && $_POST["deskripsi_surat"] <> "")

													$whr .= " AND a.deskripsi_surat LIKE '%" . $_POST["deskripsi_surat"] . "%'";

												if(isset($_POST["id_skpd_pengirim"]) && $_POST["id_skpd_pengirim"] <> 0)

													$whr .= " AND a.id_skpd_tujuan = '" . $_POST["id_skpd_pengirim"] . "'";



												$sql = "SELECT 

												a.*, b.unit_kerja, 'Siap Untuk Pengiriman' AS kondisi, 'Kepala Badan' AS asal, 0 AS id_disposisi,

												1 AS keadaan, c.kode_masalah, d.kode

												FROM 

												myapp_maintable_suratkeluar a

												LEFT JOIN myapp_reftable_unitkerja b ON a.id_skpd_tujuan = b.id_unit_kerja

												LEFT JOIN myapp_reftable_masalah c ON a.id_masalah = c.id_masalah

												LEFT JOIN myapp_reftable_jenissurat d ON a.id_jenis_surat = d.id_jenis_surat

												WHERE

												a.status = 3 " . $whr . "

												ORDER BY 

												id DESC";





											}else if(isset($_POST["ftype"]) && $_POST["ftype"] == 2){ 

												if(isset($_POST["id"]) && $_POST["id"] <> "")

													$whr .= " AND a.id = '" . $_POST["id"] . "'";

												if(isset($_POST["no_surat"]) && $_POST["no_surat"] <> "")

													$whr .= " AND a.no_surat LIKE '%" . $_POST["no_surat"] . "%'";

												if(isset($_POST["tgl_surat_dari"]) && isset($_POST["tgl_surat_sampai"]) && $_POST["tgl_surat_dari"] <> "" && $_POST["tgl_surat_sampai"] <> "")

													$whr .= " AND a.tgl_surat BETWEEN '" . $_POST["tgl_surat_dari"] . "' AND '" . $_POST["tgl_surat_sampai"] . "'";

												if(isset($_POST["tgl_terima_dari"]) && isset($_POST["tgl_terima_sampai"]) && $_POST["tgl_terima_dari"] <> "" && $_POST["tgl_terima_sampai"] <> "")

													$whr .= " AND a.tgl_kirim BETWEEN '" . $_POST["tgl_terima_dari"] . "' AND '" . $_POST["tgl_terima_sampai"] . "'";

												if(isset($_POST["perihal_surat"]) && $_POST["perihal_surat"] <> "")

													$whr .= " AND a.perihal_surat LIKE '%" . $_POST["perihal_surat"] . "%'";

												if(isset($_POST["deskripsi_surat"]) && $_POST["deskripsi_surat"] <> "")

													$whr .= " AND a.deskripsi_surat LIKE '%" . $_POST["deskripsi_surat"] . "%'";

												if(isset($_POST["id_skpd_pengirim"]) && $_POST["id_skpd_pengirim"] <> 0)

													$whr .= " AND a.id_skpd_tujuan = '" . $_POST["id_skpd_pengirim"] . "'";



												$sql = "SELECT 

												a.*, b.unit_kerja, 'Siap Untuk Pengiriman' AS kondisi, 'Kepala Badan' AS asal, 0 AS id_disposisi,

												1 AS keadaan, c.kode_masalah, d.kode, f.no_surat

												FROM 

												myapp_maintable_suratkeluar a

												LEFT JOIN myapp_reftable_unitkerja b ON a.id_skpd_tujuan = b.id_unit_kerja

												LEFT JOIN myapp_reftable_masalah c ON a.id_masalah = c.id_masalah

												LEFT JOIN myapp_reftable_jenissurat d ON a.id_jenis_surat = d.id_jenis_surat

												LEFT JOIN myapp_maintable_balasan e ON a.id = e.id_surat_keluar

												LEFT JOIN myapp_maintable_suratmasuk f ON e.id_surat_masuk = f.id

												WHERE

												a.status = 3 " . $whr . "

												GROUP BY

												a.id

												ORDER BY

												f.id DESC";

											}

        //echo($sql);

											$res = $con->query($sql);

											$ctr = 0;

											$res->data_seek(0);

											while($ds = $res->fetch_assoc()){

												$nosur = explode("/", $ds["no_surat"]);



												$ctr++;

												echo("<tr>");

												$kondisi = "belumdibaca";

												if($ds["kondisi"] == 2)

													$kondisi = "telahdibaca";

												echo("<td align='center'>" . $ctr . "</td>");

												echo("<td align='center'>" . no_register($ds["id"]) . "</td>");

												echo("<td>" . $ds["no_surat"] . "</td>");

												echo("<td>" . tglindonesia($ds["tgl_surat"]) . "</td>");

												echo("<td>" . tglindonesia($ds["tgl_kirim"]) . "</td>");

												echo("<td>" . $ds["perihal_surat"] . "</td>");

												echo("<td>" . $ds["unit_kerja"] . "</td>");

												echo("<td>" . $ds["asal"] . "</td>");

												echo("<td>" . konversiKeadaanSK($ds["keadaan"]) . "</td>");

												if($ds["no_surat"] == '')

												{

													echo "

													<td colspan='5'><i>Belum Dinomori</i></td>

													<td td class='ilang'></td>

													<td td class='ilang'></td>

													<td td class='ilang'></td>

													<td td class='ilang'></td>";

												}

												else

												{

													echo("<td align='center'>");

													echo("<img src='image/info-32.png' width='18px' class='linkimage' title='Detail Surat Keluar' onclick='lihat_detail_sk(" . $ds["id"] . ", " . $ds["id_disposisi"] . ");'>");

													echo("</td>");

													echo("<td align='center'>");

													echo("<img src='image/icon-disposisi.png' width='18px' class='linkimage' title='Daftar catatan disposisi' onclick='lihat_cadis_sk(" . $ds["id"] . ", " . $ds["id_disposisi"] . ");'>");

													echo("</td>");

													echo("<td align='center'>");

													echo("<a href='./?mod=file_final_surat_keluar&id=". $ds["id"] ."' id='uploadBerkas_". $ds["id"] ."'>

														<img src='image/upload.png' width='18px' class='linkimage' title='Upload File Final' ></a>");

													echo("</td>");

													echo("<td align='center'>");

													echo("<img src='image/Attachment-32.png' width='18px' class='linkimage' title='File yang dilampirkan' onclick='lihat_file_sk(" . $ds["id"] . ", " . $ds["id_disposisi"] . ");'>");

													echo("</td>");

													echo("<td align='center'>");

													echo("<img src='image/send_32.png' width='18px' class='linkimage' title='Selesai Dan Proses Surat Keluar Telah Berakhir' onclick='disposisi(" . $ds["id"] . ", " . $ds["id_disposisi"] . ");'>");

													echo("</td>");

												}

												

												echo("</tr>\n");



												echo "

												<tr id='div_if'>

												<span style='display:none;' id='load_text'></span>
												<td td class='ilang'></td>
												
												<td colspan='14'> <div id='div_cek_".$ds["id"]."'></div>

												<td td class='ilang'></td>

												<td td class='ilang'></td>

												<td td class='ilang'></td>

												<td td class='ilang'></td>

												<td td class='ilang'></td>

												<td td class='ilang'></td>

												<td td class='ilang'></td>


												<td td class='ilang'></td>

												<td td class='ilang'></td>

												<td td class='ilang'></td>

												<td td class='ilang'></td>

												<td td class='ilang'></td>

												<td td class='ilang'></td>

												</tr>

												";	

												?>	

												<!-- Script javascript untuk menampilkan dialog upload File -->

												<script>

													var id = '<?php echo $ds["id"];?>';



													$("#uploadBerkas_"+id).fancybox({

														'width'				: '80%',

														'height'			: '60%',

														'autoScale'			: false,

														'transitionIn'		: 'none',

														'transitionOut'		: 'none',

														'overlayShow'	: false,

														'type'				: 'iframe'

													});

												</script>

												<?php

											}



		$res->close(); // tutup koneksi ke dalam database

		?>

	</tbody>

</table>

</fieldset>    

</div> 

<!-- /.box-body -->

</div>   <br>     

</div>

<!-- ./col -->

</div> 

<!-- /.row (main row) -->



</section>

<!-- /.content -->

</div>

<!-- /.content-wrapper -->

<?php include 'isi/capekkali/footer.php';?>



<!-- Control Sidebar -->



<!-- /.control-sidebar -->

  <!-- Add the sidebar's background. This div must be placed

  	immediately after the control sidebar -->

  	<div class="control-sidebar-bg"></div>

  </div>

  <!-- ./wrapper -->



  <!-- jQuery 3 -->

  <script src="bower_components/jquery/dist/jquery.min.js"></script>



  <script src="cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>

  <!-- Bootstrap 3.3.7 -->

  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

  <!-- dtt -->

  <script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

  <script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

  <!-- Select2 -->

  <script src="bower_components/select2/dist/js/select2.full.min.js"></script>

  <!-- InputMask -->

  <script src="plugins/input-mask/jquery.inputmask.js"></script>

  <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>

  <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>

  <!-- date-range-picker -->

  <script src="bower_components/moment/min/moment.min.js"></script>

  <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

  <!-- bootstrap datepicker -->

  <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

  <!-- bootstrap color picker -->

  <script src="bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

  <!-- bootstrap time picker -->

  <script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>

  <!-- SlimScroll -->

  <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

  <!-- iCheck 1.0.1 -->

  <script src="plugins/iCheck/icheck.min.js"></script>

  <!-- FastClick -->

  <script src="bower_components/fastclick/lib/fastclick.js"></script>

  <!-- AdminLTE App -->

  <script src="dist/js/adminlte.min.js"></script>

  <!-- AdminLTE for demo purposes -->

  <script src="dist/js/demo.js"></script>

  <!-- Page script -->



  <script type="text/javascript">

  	$(document).ready(function(){

  		$("#expand").click(function(){

  			$("#bodyFilter").slideToggle(500);

  		});



  		$("#tgl_surat_dari").datepicker({

  			dateFormat: "yy-mm-dd",

  			changeMonth: true,

  			changeYear: true

  		});

  		$("#tgl_terima_dari").datepicker({

  			dateFormat: "yy-mm-dd",

  			changeMonth: true,

  			changeYear: true

  		});



  		$("#tgl_surat_sampai").datepicker({

  			dateFormat: "yy-mm-dd",

  			changeMonth: true,

  			changeYear: true

  		});

  		$("#tgl_terima_sampai").datepicker({

  			dateFormat: "yy-mm-dd",

  			changeMonth: true,

  			changeYear: true

  		});



  	});

  	function disposisi(id_surat, id_disposisi){

  		var t = document.getElementById("div_cek_"+id_surat);

  		if(t.innerHTML != ''){

  			t.innerHTML = '';

  		}else

  		t.innerHTML='<iframe src="./isi/panel/modal/dialog_frm_dsp.php?id_surat_masuk='+id_surat+'&id_disposisi='+id_disposisi+'" width="100%" style="height:1300px" frameborder="0" ' + 

  		'scrolling="no" id="iframe_detail"></iframe>';	

  	}



  	

  	function kestaff(id_bidang){

    //$("#staff").html(id_bidang);

    $("#staff").html("Loading . . .");

    $.ajax({

    	type: "GET",

    	url: "ajax/disposisi_kasubbid.php",

    	data: "id=" + id_bidang,

    	success: function(r){

            //alert(r);

            $("#staff").html(r);

        }

    });

}

</script>

<script type="text/javascript" src="./libraries/main.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script> 
	$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#example2 tfoot th').each( function () {
    	var title = $(this).text();
    	$(this).html( '<input type="text" placeholder="Cari '+title+'" />' );
    } );
    
    // DataTable
    var table = $('#example2').DataTable(
    {
    	'paging'      : true,
    	'lengthChange': true,
    	'searching'   : true,
    	'ordering'    : false,
    	'info'        : false,
    	'autoWidth'   : false,
    	rowReorder: {
    		selector: 'td:nth-child(2)'
    	},
    	responsive: true
    });
    
    // Apply the search
    table.columns().every( function () {
    	var that = this;
    	
    	$( 'input', this.footer() ).on( 'keyup change', function () {
    		if ( that.search() !== this.value ) {
    			that
    			.search( this.value )
    			.draw();
    		}
    	} );
    } );
} );
</script>

</body>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
<script>
 if ('serviceWorker' in navigator) {
    console.log("Will the service worker register?");
    navigator.serviceWorker.register('service-worker.js')
      .then(function(reg){
        console.log("Yes, it did.");
     }).catch(function(err) {
        console.log("No it didn't. This happened:", err)
    });
 }
</script>
</html>




