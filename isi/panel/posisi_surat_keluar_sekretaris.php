<?php
    // tentukan user level dan alamat kembalinya
$main = "";

switch($_SESSION["id_level"]){
  case 10 :
  $main = "../index.php?mod=stafkasipentaker&opt=utama";
  break;      
  case 14  :
  $main = "../index.php?mod=stafkasiwasnaker&opt=utama";
  break;
  case 21  :
  case 25  :
  $main = "../index.php?mod=stafkasihubsaker&opt=utama";
  break;
  case 24 :
  $main = "../index.php?mod=stafkasilatih&opt=utama";
  break;
}
?>




<!DOCTYPE html>
<html>
<head>  
  <meta name="theme-color" content="#00923F">
<link rel="manifest" href="manifest.json">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>::: DINAS KETENAGAKERJAAN PEMERINTAH KOTA MEDAN :::</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">

  <!-- dtt -->
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
   <!-- Morris chart -->
   <link rel="stylesheet" href="bower_components/morris.js/morris.css">
   <!-- jvectormap -->
   <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
   <!-- Date Picker -->
   <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
   <!-- Daterange picker -->
   <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
   <!-- bootstrap wysihtml5 - text editor -->
   <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link rel="stylesheet" href="dist/css/custom.css">
<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">

</head>
<body class="hold-transition skin-green sidebar-mini">
  <div class="wrapper">

    <?php include 'isi/capekkali/header.php';?>
    <!-- Left side column. contains the logo and sidebar -->
    <?= $_SESSION['menu'] ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">

        <h1>

          PROSES SURAT KELUAR

        </h1>

        <ol class="breadcrumb">

          <li><a href="./"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li>Surat Keluar</li>

          <li class="active">Proses Surat Keluar</li>

        </ol>

      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) --> 
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)--> 
            <!-- /.nav-tabs-custom -->

            <!-- Chat box -->
            <div class="box box-warning"> 
              <div class="box-body">
                <div id='content'>  
                  <fieldset> 

                    <table id="example2" class="listingtable table table-bordered table-striped">
                     <tfoot>
                      <tr> 
                        <th>NO.</th>
                        <th>NO. REGISTER</th>
                        <th>NO. SURAT</th>
                        <th>TGL. SURAT</th>
                        <th>TGL. TERIMA</th>
                        <th>PERIHAL</th>
                        <th>UNIT PENGIRIM</th>
                        <th>ASAL DISPOSISI</th>
                        <th>TIPE DISPOSISI</th>
                        <th style="display: none">&nbsp;</th>
                        <th style="display: none">&nbsp;</th>
                        <th style="display: none">&nbsp;</th>
                        <th style="display: none">&nbsp;</th>
                        <th style="display: none">&nbsp;</th>
                      </tr>
                    </tfoot>
                    <thead>
                      <tr> 
                        <th>NO.</th>
                        <th>NO. REGISTER</th>
                        <th>NO. SURAT</th>
                        <th>TGL. SURAT</th>
                        <th>TGL. TERIMA</th>
                        <th>PERIHAL</th>
                        <th>UNIT PENGIRIM</th>
                        <th>ASAL DISPOSISI</th>
                        <th>TIPE DISPOSISI</th>
                        <th width='30px'>DETAIL SURAT YG DIBALAS</th> 
                        <th width='30px'>DETAIL</th> 
                        <th width='30px'>RIWAYAT</th> 
                        <th width='30px'>LAMPIRAN</th> 
                        <th width='30px'>DISPOSISI</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php
                      $whr = "";
                      if($_POST["id"] <> "")
                        $whr .= " AND a.id = '" . $_POST["id"] . "'";
                      if($_POST["no_surat"] <> "")
                        $whr .= " AND a.no_surat LIKE '%" . $_POST["no_surat"] . "%'";
                      if($_POST["tgl_surat_dari"] <> "" && $_POST["tgl_surat_sampai"] <> "")
                        $whr .= " AND a.tgl_surat BETWEEN '" . $_POST["tgl_surat_dari"] . "' AND '" . $_POST["tgl_surat_sampai"] . "'";
                      if($_POST["tgl_terima_dari"] <> "" && $_POST["tgl_terima_sampai"] <> "")
                        $whr .= " AND a.tgl_kirim BETWEEN '" . $_POST["tgl_terima_dari"] . "' AND '" . $_POST["tgl_terima_sampai"] . "'";
                      if($_POST["perihal_surat"] <> "")
                        $whr .= " AND a.perihal_surat LIKE '%" . $_POST["perihal_surat"] . "%'";
                      if($_POST["deskripsi_surat"] <> "")
                        $whr .= " AND a.deskripsi_surat LIKE '%" . $_POST["deskripsi_surat"] . "%'";
                      if($_POST["id_skpd_pengirim"] <> 0)
                        $whr .= " AND a.id_skpd_tujuan = '" . $_POST["id_skpd_pengirim"] . "'";
                      
                      $res = mysql_query("SELECT 
                        a.*, b.unit_kerja, c.status AS kondisi, d.nama_level AS asal, c.id AS id_disposisi,c.tolak,
                        c.keadaan, e.id_surat_masuk
                        FROM 
                        myapp_maintable_suratkeluar a
                        LEFT JOIN myapp_reftable_unitkerja b ON a.id_skpd_tujuan = b.id_unit_kerja
                        LEFT JOIN myapp_disptable_suratkeluar c ON a.id = c.id_surat_keluar
                        LEFT JOIN myapp_reftable_levelpengguna d ON c.id_level_asal = d.id
                        LEFT JOIN myapp_maintable_balasan e ON a.id = e.id_surat_keluar
                        WHERE
                        c.id_level_tujuan = '" . $_SESSION["id_level"] . "' AND c.status < 3 " . $whr . "
                        GROUP BY
                        a.id
                        ORDER BY 
                        id DESC"); 
                      $ctr = 0;
                      while($ds = mysql_fetch_array($res)){
                        $ctr++;
                        $class="";
                        if($ds['tolak'] == 1)
                        {
                          $class = 'class="danger"';
                        }

                        echo("<tr ".$class.">");
                        $kondisi = "belumdibaca";
                        if($ds["kondisi"] == 2)
                          $kondisi = "telahdibaca"; 
                        echo("<td align='center'>" . $ctr . "</td>");
                        echo("<td align='center'>" . no_register($ds["id"]) . "</td>");
                        echo("<td>" . $ds["no_surat"] . "</td>");
                        echo("<td>" . tglindonesia($ds["tgl_surat"]) . "</td>");
                        echo("<td>" . tglindonesia($ds["tgl_terima"]) . "</td>");
                        echo("<td>" . $ds["perihal_surat"] . "</td>");
                        echo("<td>" . $ds["unit_kerja"] . "</td>");
                        echo("<td>" . $ds["asal"] . "</td>");
                        echo("<td>" . konversiKeadaanSK($ds["keadaan"]) . "</td>");
                        echo("<td align='center'>");
                        echo("<img src='image/icon/con_email.png' width='18px' class='linkimage' title='Data Surat Masuk Yang Dibalas' onclick='lihat_data_yg_dibalas(" . $ds["id"] . ", " . $ds["id_disposisi"] . ");'>");
                        echo("</td>");
                        echo("<td align='center'>");
                        echo("<img src='image/info-32.png' width='18px' class='linkimage' title='Detail Surat Keluar' onclick='lihat_detail_sk(" . $ds["id"] . ", " . $ds["id_disposisi"] . ");'>");
                        echo("</td>");
                        echo("<td align='center'>");
                        echo("<img src='image/icon-disposisi.png' width='18px' class='linkimage' title='Daftar catatan disposisi' onclick='lihat_cadis_sk(" . $ds["id"] . ", " . $ds["id_disposisi"] . ");'>");
                        echo("</td>");
                        echo("<td align='center'>");
                        echo("<img src='image/Attachment-32.png' width='18px' class='linkimage' title='File yang dilampirkan' onclick='lihat_file_sk(" . $ds["id"] . ", " . $ds["id_disposisi"] . ");'>");
                        echo("</td>");
                        echo("<td align='center'>");
                        echo("<img src='image/send_32.png' width='18px' class='linkimage' title='Lanjutkan Disposisi' onclick='disposisi(" . $ds["id"] . ", " . $ds["id_disposisi"] . ");'>");
                        echo("</td>");
                        echo("</tr>");
                        
                        echo "
                        <tr id='div_if'>
                        <span style='display:none;' id='load_text'></span>
                        <td class='ilang'></td>
                        
                        <td colspan='15'> <div id='div_cek_".$ds["id"]."'></div></td>

                        <td class='ilang'></td>
                        <td class='ilang'></td>
                        <td class='ilang'></td>
                        <td class='ilang'></td>
                        <td class='ilang'></td>
                        <td class='ilang'></td>
                        <td class='ilang'></td>
                        <td class='ilang'></td>
                        <td class='ilang'></td>
                        <td class='ilang'></td>
                        <td class='ilang'></td>
                        <td class='ilang'></td>
                        <td class='ilang'></td>
                        </tr>
                        ";  
                      }
                      ?>
                    </tbody>
                  </table>
                </fieldset>   

              </div>
            </div>
            <!-- /.chat --> 
          </div>
          <!-- /.box (chat box) -->

          <!-- TO DO List --> 
          <!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)--> 
        </div>
        <!-- /.row (main row) -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include 'isi/capekkali/footer.php';?>

    <!-- Control Sidebar -->

    <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
   <div class="control-sidebar-bg"></div>
 </div>
 <!-- ./wrapper -->

 <!-- jQuery 3 -->
 <script src="bower_components/jquery/dist/jquery.min.js"></script>
 <!-- jQuery UI 1.11.4 -->
 <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
 <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
 <script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- dtt -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#expand").click(function(){
      $("#bodyFilter").slideToggle(500);
    });
    
    $("#tgl_surat_dari").datepicker({
      dateFormat: "yy-mm-dd",
      changeMonth: true,
      changeYear: true
    });
    $("#tgl_terima_dari").datepicker({
      dateFormat: "yy-mm-dd",
      changeMonth: true,
      changeYear: true
    });
    
    $("#tgl_surat_sampai").datepicker({
      dateFormat: "yy-mm-dd",
      changeMonth: true,
      changeYear: true
    });
    $("#tgl_terima_sampai").datepicker({
      dateFormat: "yy-mm-dd",
      changeMonth: true,
      changeYear: true
    });
  });
  function disposisi(id_surat, id_disposisi){
   var t = document.getElementById("div_cek_"+id_surat);
   if(t.innerHTML != ''){
     t.innerHTML = '';
   }else
   t.innerHTML='<iframe src="./isi/panel/modal/dialog_posisi_sk_sekretaris.php?id_surat_keluar='+id_surat+'&id_disposisi='+id_disposisi+'" width="100%" onload="resizeIframe(this)" frameborder="0" ' + 
   'scrolling="no" id="iframe_detail"></iframe>';  
 }

</script>
<script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script> 
  $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#example2 tfoot th').each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Cari '+title+'" />' );
    } );
    
    // DataTable
    var table = $('#example2').DataTable(
    {
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : false,
      rowReorder: {
        selector: 'td:nth-child(2)'
      },
      responsive: true
    });
    
    // Apply the search
    table.columns().every( function () {
      var that = this;
      
      $( 'input', this.footer() ).on( 'keyup change', function () {
        if ( that.search() !== this.value ) {
          that
          .search( this.value )
          .draw();
        }
      } );
    } );
  } );
</script>
</body>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
<script>
 if ('serviceWorker' in navigator) {
    console.log("Will the service worker register?");
    navigator.serviceWorker.register('service-worker.js')
      .then(function(reg){
        console.log("Yes, it did.");
     }).catch(function(err) {
        console.log("No it didn't. This happened:", err)
    });
 }
</script>
</html>
   


