  

<!DOCTYPE html>

<html>

<head>  
  <meta name="theme-color" content="#00923F">
<link rel="manifest" href="manifest.json">

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>::: DINAS KETENAGAKERJAAN PEMERINTAH KOTA MEDAN :::</title>




  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->

  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">

  <!-- Font Awesome -->

  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">

  <!-- Ionicons -->

  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">



  <!-- dtt -->

  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- Theme style -->

  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins

    folder instead of downloading all of them to reduce the load. -->

    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!-- Morris chart -->

    <link rel="stylesheet" href="bower_components/morris.js/morris.css">

    <!-- jvectormap -->

    <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">

    <!-- Date Picker -->

    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- Daterange picker -->

    <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">

    <!-- bootstrap wysihtml5 - text editor -->

    <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<![endif]-->



<!-- Google Font -->

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<link rel="stylesheet" href="dist/css/custom.css">

<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">

</head>

<body class="hold-transition skin-green sidebar-mini">

  <div class="wrapper">



    <?php include 'isi/capekkali/header.php';?>

    <!-- Left side column. contains the logo and sidebar -->

    <?= $_SESSION['menu']?>



    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">

      <!-- Content Header (Page header) -->

      <section class="content-header">

        <h1>

          LAPORAN REKAP SURAT KELUAR YANG MENUNGGU ARSIP

        </h1>

        <ol class="breadcrumb">

          <li><a href="./"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li>Laporan</li>

          <li class="active">Laporan Rekap Surat Keluar yang Menunggu Arsip</li>

        </ol>

      </section>



      <!-- Main content -->

      <section class="content">

        <!-- Small boxes (Stat box) -->

        <div class="row">



          <div class="col-md-12"> 

            <div class="box box-warning"> 

              <div class="box-body"> 
               <a class="btn btn-success" href="./libraries/mpdf/cetak_skw_arsip.php">Cetak Laporan</a> <a class="btn btn-success" href="./libraries/simpleexcel/psk.php">Ekspor ke XLS</a><br>
               <br>
               <table id="example2" class="listingtable table table-bordered table-striped">

                <tfoot>

                  <tr class="headertable">

                    <th width='40px'>NO.</th>

                    <th width='100px'>NO. REGISTER</th>

                    <th width='250px'>NO. SURAT</th>

                    <th width='150px'>TGL. SURAT</th>

                    <th width='150px'>TGL. KIRIM</th>

                    <th>PERIHAL</th>

                    <th width='250px'>UNIT TUJUAN</th>

                    <th style="display: none" width='20px'>&nbsp;</th>

                    <th style="display: none" width='20px'>&nbsp;</th>

                    <th style="display: none" width='20px'>&nbsp;</th>

                  </tr>

                </tfoot>

                <thead>

                  <tr class="headertable">

                    <th width='40px'>NO.</th>

                    <th width='100px'>NO. REGISTER</th>

                    <th width='250px'>NO. SURAT</th>

                    <th width='150px'>TGL. SURAT</th>

                    <th width='150px'>TGL. KIRIM</th>

                    <th>PERIHAL</th>

                    <th width='250px'>UNIT TUJUAN</th>

                    <th width='30px'>DETAIL</th> 
                    <th width='30px'>RIWAYAT</th> 
                    <th width='30px'>LAMPIRAN</th> 

                  </tr>

                </thead>

                <tbody>

                  <?php

                  $whr = "";

                  if($_POST["id"] <> "")

                    $whr .= " AND a.id = '" . $_POST["id"] . "'";

                  if($_POST["no_surat"] <> "")

                    $whr .= " AND a.no_surat LIKE '%" . $_POST["no_surat"] . "%'";

                  if($_POST["tgl_surat_dari"] <> "" && $_POST["tgl_surat_sampai"] <> "")

                    $whr .= " AND a.tgl_surat BETWEEN '" . $_POST["tgl_surat_dari"] . "' AND '" . $_POST["tgl_surat_sampai"] . "'";

                  if($_POST["tgl_terima_dari"] <> "" && $_POST["tgl_terima_sampai"] <> "")

                    $whr .= " AND a.tgl_kirim BETWEEN '" . $_POST["tgl_terima_dari"] . "' AND '" . $_POST["tgl_terima_sampai"] . "'";

                  if($_POST["perihal_surat"] <> "")

                    $whr .= " AND a.perihal_surat LIKE '%" . $_POST["perihal_surat"] . "%'";

                  if($_POST["deskripsi_surat"] <> "")

                    $whr .= " AND a.deskripsi_surat LIKE '%" . $_POST["deskripsi_surat"] . "%'";

                  if($_POST["id_skpd_pengirim"] <> 0)

                    $whr .= " AND a.id_skpd_tujuan = '" . $_POST["id_skpd_pengirim"] . "'";



                  $ds_urutan = mysql_fetch_array(mysql_query("SELECT * FROM myapp_reftable_levelpengguna WHERE id='" . $_SESSION["id_level"] . "'"));

                  $urutan = $ds_urutan["urutan"];



                  $tmbh = " (c.id_level_tujuan = '" . $_SESSION["id_level"] . "' ";

                  if($urutan == 0){

                    $tmbh = '(a.id_surat_masuk <> ""';
                    $whr = '';

                  }
                  else if($urutan == 2){

                    $res_bawahan = mysql_query("SELECT * FROM myapp_reftable_levelpengguna WHERE atasan='" . $_SESSION["id_level"] . "'");

                    while($ds_bawahan = mysql_fetch_array($res_bawahan)){

                      $tmbh .= " OR ";

                      $tmbh .= " c.id_level_tujuan = '" . $ds_bawahan["id"] . "' ";



                    }

                  }else if($urutan == 3){

                    $res_bawahan = mysql_query("SELECT * FROM myapp_reftable_levelpengguna WHERE atasan='" . $_SESSION["atasan"] . "' AND urutan=4");

                    while($ds_bawahan = mysql_fetch_array($res_bawahan)){

                      $tmbh .= " OR ";

                      $tmbh .= " (c.id_level_asal = '" . $_SESSION["id_level"] . "' AND c.id_level_tujuan = '" . $ds_bawahan["id"] . "') ";

                      $tmbh .= " OR ";

                      $tmbh .= " (c.id_level_asal = '" . $ds_bawahan["id"] . "' AND c.id_level_tujuan = '" . $_SESSION["id_level"] . "') ";

                    }

                    $res_bawahan_langsung = mysql_query("SELECT * FROM myapp_maintable_pengguna WHERE level_kasubbid='" . $_SESSION["id_level"] . "'");

                    while($ds_bawahan_langsung = mysql_fetch_array($res_bawahan_langsung)){

                      $tmbh .= " OR ";

                      $tmbh .= " (c.id_level_asal = '7' AND c.id_pengguna_tujuan = '" . $ds_bawahan_langsung["id"] . "') ";

                    }

                  }

                  $tmbh .= " ) ";


                  $res = mysql_query("SELECT 

                   a.*, b.unit_kerja,

                   CASE

                   WHEN a.no_surat = '' THEN '[[ Belum ditentukan ]]'

                   ELSE a.no_surat

                   END AS nomor_surat,

                   CASE

                   WHEN a.tgl_kirim = '0000-00-00' THEN '[[ Belum ditentukan ]]'

                   ELSE a.tgl_kirim

                   END AS tgl_pengiriman

                   FROM 

                   myapp_maintable_suratkeluar a

                   LEFT JOIN myapp_reftable_unitkerja b ON a.id_skpd_tujuan = b.id_unit_kerja

                   LEFT JOIN myapp_disptable_suratkeluar c ON a.id = c.id_surat_keluar
                   LEFT JOIN myapp_archivetable_suratkeluar d ON a.id = d.id_surat_keluar


                   WHERE

                   1 AND " . $whr . " " . $tmbh . " AND a.status = '4' AND d.id_surat_keluar is null

                   GROUP BY

                   a.id

                   ORDER BY 

                   a.id DESC");
                  



                  $ctr = 0;

                  while($ds = mysql_fetch_array($res)){

                    $ctr++;

                    echo("<tr>");

                    echo("<td align='center'>" . $ctr . "</td>");

                    echo("<td align='center'>" . no_register($ds["id"]) . "</td>");

                    echo("<td>" . $ds["nomor_surat"] . "</td>");

                    echo("<td>" . tglindonesia($ds["tgl_surat"]) . "</td>");

                    echo("<td>" . tglindonesia($ds["tgl_pengiriman"]) . "</td>");

                    echo("<td>" . $ds["perihal_surat"] . "</td>");

                    echo("<td>" . $ds["unit_kerja"] . "</td>");

                    echo("<td align='center'>");

                    echo("<img src='image/information_32.png' width='18px' class='linkimage' title='Detail Surat Keluar' onclick='lihat_detail_sk(" . $ds["id"] . ");'>");

                    echo("</td>");

                    echo("<td align='center'>");

                    echo("<img src='image/icon-disposisi.png' width='18px' class='linkimage' title='Daftar catatan disposisi' onclick='lihat_cadis_sk(" . $ds["id"] . ");'>");

                    echo("</td>");

                    echo("<td align='center'>");

                    echo("<img src='image/Attachment-32.png' width='18px' class='linkimage' title='File yang dilampirkan' onclick='lihat_file_sk(" . $ds["id"] . ");'>");

                    echo("</td>");

                    echo("</tr>");

                    echo "

                    <tr id='div_if'>
                    
                    <span style='display:none;' id='load_text'></span>
                    <td class='ilang'></td>

                    <td colspan='11'> <div id='div_cek_".$ds["id"]."'></div></td>

                    <td class='ilang'></td>


                    <td class='ilang'></td>

                    <td class='ilang'></td>

                    <td class='ilang'></td>

                    <td class='ilang'></td>

                    <td class='ilang'></td>

                    <td class='ilang'></td>

                    <td class='ilang'></td>

                    <td class='ilang'></td>

                    </tr>

                    ";  

                  }

                  ?>

                </tbody>

              </table></div>

            </div> 

            <!-- /.box-body -->

          </div>   <br>     

        </div>

        <!-- ./col -->

      </div> 

      <!-- /.row (main row) -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

  <?php include 'isi/capekkali/footer.php';?>



  <!-- Control Sidebar --> 

  <!-- Add the sidebar's background. This div must be placed

    immediately after the control sidebar -->

    <div class="control-sidebar-bg"></div>

  </div>

  <!-- ./wrapper -->



  <!-- jQuery 3 -->

  <script src="bower_components/jquery/dist/jquery.min.js"></script>



  <script src="cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>

  <!-- Bootstrap 3.3.7 -->

  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



  <!-- dtt -->

  <script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

  <script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

  <!-- Select2 -->

  <script src="bower_components/select2/dist/js/select2.full.min.js"></script>

  <!-- InputMask -->

  <script src="plugins/input-mask/jquery.inputmask.js"></script>

  <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>

  <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>

  <!-- date-range-picker -->

  <script src="bower_components/moment/min/moment.min.js"></script>

  <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

  <!-- bootstrap datepicker -->

  <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

  <!-- bootstrap color picker -->

  <script src="bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

  <!-- bootstrap time picker -->

  <script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>

  <!-- SlimScroll -->

  <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

  <!-- iCheck 1.0.1 -->

  <script src="plugins/iCheck/icheck.min.js"></script>

  <!-- FastClick -->

  <script src="bower_components/fastclick/lib/fastclick.js"></script>

  <!-- AdminLTE App -->

  <script src="dist/js/adminlte.min.js"></script>

  <!-- AdminLTE for demo purposes -->

  <script src="dist/js/demo.js"></script>

  <!-- Page script -->

  <script type="text/javascript">

    function hapus(id){

      jConfirm("Anda yakin akan menghapus data ini?", "PERHATIAN", function(r){

        if(r){

            //alert("dihapus " + id);

            document.location.href = "php/hapus_surat_keluar.php?id=" + id;

          }

        });

    }

    function edit(id){

      document.location.href = "?mod=edit_surat_keluar&id=" + id;

    }



    $(document).ready(function(){

      $("#expand").click(function(){

        $("#bodyfilter").slideToggle(500);

      });

      

      $("#tgl_surat_dari").datepicker({

        dateFormat: "yy-mm-dd",

        changeMonth: true,

        changeYear: true

      });

      $("#tgl_terima_dari").datepicker({

        dateFormat: "yy-mm-dd",

        changeMonth: true,

        changeYear: true

      });

      

      $("#tgl_surat_sampai").datepicker({

        dateFormat: "yy-mm-dd",

        changeMonth: true,

        changeYear: true

      });

      $("#tgl_terima_sampai").datepicker({

        dateFormat: "yy-mm-dd",

        changeMonth: true,

        changeYear: true

      });

    });

  </script>

  <script type="text/javascript" src="./libraries/main.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script> 
  $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#example2 tfoot th').each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Cari '+title+'" />' );
    } );
    
    // DataTable
    var table = $('#example2').DataTable(
      {
        'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : false,
       rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
      });
    
    // Apply the search
    table.columns().every( function () {
      var that = this;
      
      $( 'input', this.footer() ).on( 'keyup change', function () {
        if ( that.search() !== this.value ) {
          that
          .search( this.value )
          .draw();
        }
      } );
    } );
  } );
</script>

</body>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
<script>
 if ('serviceWorker' in navigator) {
    console.log("Will the service worker register?");
    navigator.serviceWorker.register('service-worker.js')
      .then(function(reg){
        console.log("Yes, it did.");
     }).catch(function(err) {
        console.log("No it didn't. This happened:", err)
    });
 }
</script>
</html>






